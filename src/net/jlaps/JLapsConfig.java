/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * config store manager for  the application
 * it manages jlaps.properties and it default values
 * 
 * @author lskokan
 */
public class JLapsConfig {

    
    final static String JLAPS_PROPERTY="jlaps.properties";
    final static String JLAPS_PROPERTY_COMMENT="JLaps config file. Do not edit!";
    Properties p;
    
    /**
     * open properties. if it does not exists, creates it from default values
     */
    public JLapsConfig() {
        try {
            p = new Properties();
            p.load(new FileInputStream(JLAPS_PROPERTY));
            
        } catch (Exception ex) {
            System.err.println("Missing " + new File(JLAPS_PROPERTY).getAbsolutePath());
            Logger.getLogger(JLapsConfig.class.getName()).log(Level.SEVERE, null, ex);
            fillDefaults();
        }
        
    }
    
    /**
     * saves the key into properties file
     * @param key to save
     * @param value to be stored for the key
     */
    public void saveKey(String key, String value) {
        try {
            p.put(key, value);
            p.store(new FileOutputStream(JLAPS_PROPERTY), JLAPS_PROPERTY_COMMENT);
        } catch (Exception ex) {
            Logger.getLogger(JLapsConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    /**
     * gets a key value from properties file
     * @param key to get value for
     * @return vlue of the key
     */
    public String getKey(String key) {
        if ( p.getProperty(key)==null) {
            fillDefaults();
        }
        return p.getProperty(key);
        
    }

    /**
     * has default values for all known keys. When there is no properties file,
     * class uses this to get defaults and create properties file
     */
    private void fillDefaults() {
        p = new Properties();
        p.put("AMB_PORT", "8091");
        p.put("AMB_IP","localhost");

        p.put("RACE_PREPARATION_TIME_SECS","10");
        p.put("RACE_RUNTIME_TIME_MINS","5");
        p.put("RACE_ANONUNCEMENT_TIME_MINS","2");
        p.put("FONT_SIZE","12");
        
        
        try {
            p.store(new FileOutputStream(JLAPS_PROPERTY), JLAPS_PROPERTY_COMMENT);
        } catch (Exception ex) {
            Logger.getLogger(JLapsConfig.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
    }
    
    public static void generateDefaultSpeechProperties() {
        String propFile = "speech.properties";
        File f = new File(propFile);
        
        if ( !f.exists()) {
            
            Properties speechP = new Properties();
             
            speechP.put("FreeTTSSynthEngineCentral", "com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");
            try {
                System.out.println("Generating new speech.properties into " + new File(".").getAbsolutePath());
                speechP.store(new FileOutputStream(propFile), "Generated default config");
            } catch (IOException ex) {
                Logger.getLogger(JLapsConfig.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        }
    }
}
