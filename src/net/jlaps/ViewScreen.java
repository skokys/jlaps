/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps;

/**
 *
 * @author skoky
 */
public interface ViewScreen {
    void refreshResults(String identification, Long timestamp); 
    void setAmbStatus(String msg);
    void runRaceProgressBar(int durationSecs);
    void stopRaceProgressBar();
}
