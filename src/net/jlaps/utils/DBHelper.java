/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.DefaultListModel;
import net.jlaps.utils.tableutils.RaceSelection;
import net.jlaps.entity.Category;
import net.jlaps.entity.Driver;
import net.jlaps.entity.DriverModel;
import net.jlaps.entity.GrouppedDrivers;
import net.jlaps.entity.Model;
import net.jlaps.entity.Race;
import net.jlaps.entity.RaceResult;

/**
 *
 * @author skoky
 */
public class DBHelper {

    public static List<GrouppedDrivers> getAllGrouppedDriverIdsForRace(Race todaysRace, EntityManager em) {
        List<GrouppedDrivers> g = em.createNamedQuery("GrouppedDrivers.selectByRace").setParameter("race_id", todaysRace.getId()).getResultList();

        return g;
    }

    public static Driver getDriverById(Long driverId, EntityManager em) {
        Driver d = (Driver) em.createNamedQuery("Driver.findById").setParameter("id", driverId).getSingleResult();
        return d;
    }


    public static String getPMName() {
        
        
        if ( System.getProperty("JLAPSWS") != null && System.getProperty("JLAPSWS").compareToIgnoreCase("true")==0) {
            System.out.println("Using WebStart persistence config");
            return "JLaps2PUHibernateWS";
        } else  {
            System.out.println("Using Local DB persistence config");
            return "JLaps2PUHibernateLocal";
        }
        
        
    }
    public static void checkDBConnection() {

        return;
        
/*        
        try {
            EntityManager entityManager = javax.persistence.Persistence.createEntityManagerFactory("JLaps2PUHibernate").createEntityManager();
            Query q = entityManager.createNativeQuery("SELECT CURRENT_TIMESTAMP FROM SYSIBM.SYSDUMMY1").setMaxResults(1);
            q.getResultList();
        } catch (Throwable e) {
            System.err.println("ERROR: " + e.getLocalizedMessage());
            System.err.println("Database not available. Please review META-INF/persistence.xml file.");
            System.exit(1);
        } 
  */        
    }

    public static void checkInitialData(EntityManager em) {
        Query q1 = em.createNamedQuery("Category.all");
        Query q2 = em.createNamedQuery("Race.all");
        if ( q1.getResultList().size() > 0 || q2.getResultList().size() > 0  )
            return;
        System.out.println("No initial data. Going to insert...");

        em.getTransaction().begin();
        Category c1 = new Category("1/10 onroad",4,4,3,3);
        em.persist(c1);
        em.getTransaction().commit(); 

        
        em.getTransaction().begin();
        Category c2 = new Category("1/10 offroad",4,4,3,3);
        em.persist(c2);
        String categories = c1.getId() + ";" + c2.getId();
        Race r = new Race("Demo race", "http://jlaps.sourceforge.net", new Date(), 10,categories,"1;1");
        em.persist(r);
        
        Driver d1 = new Driver("John","Oaker");
        em.persist(d1);
        Driver d2 = new Driver("Alan","Kroker");
        em.persist(d2);
        
        Model m1 = new Model("Ansmann X1","40,256","AMB","343455",c1.getId());
        m1.setDriverId(d1.getId());
        em.persist(m1);
        Model m2 = new Model("Emax","40,256","AMB","343455",c2.getId());
        m2.setDriverId(d1.getId());
        em.persist(m2);

        Model m3 = new Model("Emax","DSM","AMB","343444",c2.getId());
        m3.setDriverId(d2.getId());
        em.persist(m3);
        
        em.getTransaction().commit();

        
    }

    public static void createNewDriverAndModel(String driverFirstName, String driverSurname, String categoryNumber, String ambId, EntityManager em) {

        Query q = em.createNamedQuery("Driver.findByFirstNameAndSurname");
        List<Driver> dr = q.setParameter("firstName", driverFirstName.trim()).setParameter("secondName", driverSurname.trim()).getResultList();

        if (dr != null) {
            if (dr.size() > 0) {
                return;
            }
        }
        Driver d = new Driver();
        d.setFirstName(driverFirstName);
        d.setSecondName(driverSurname);

        if (em.getTransaction().isActive()) {
            em.getTransaction().rollback();
        }
        em.getTransaction().begin();
        em.persist(d);

        Model m = new Model();
        try {
            m.setCategory(new Long(categoryNumber));
        } catch (Exception e) {
            //ignore me
        }
        m.setDriverId(d.getId());
        m.setIdentificationId(ambId);
        em.persist(m);

        em.getTransaction().commit();

    }

    public static DefaultListModel getAllMatchingDriversForRound(
            Long raceId, RaceSelection sel, EntityManager em) {

        Query q = em.createNativeQuery("SELECT d.ID as DID, d.FIRST_NAME,d.SECOND_NAME, m.ID as MID, m.MODEL_NAME " +
                "from DRIVER d, MODEL m, CATEGORY c WHERE d.ID = m.DRIVER_ID AND c.id=m.category_id" +
                " AND m.CATEGORY_ID = :category_id" +
                " AND d.id+m.id not in(select g.driver_id+g.model_id from groupped_drivers g where" +
                " g.race_id = :race_id and g.category_id = :category_id and g.groups = :groups) AND" +
                " m.ID not in (SELECT MODEL_ID FROM GROUPPED_DRIVERS WHERE CATEGORY_ID=:category_id " +
                " AND RACE_ID = :race_id)");
        /// TBD improve SQL statement - nto to use + for IDs
        List results = q.setParameter("category_id", sel.getCategory().getId()).setParameter("race_id", raceId).setParameter("groups", sel.getGroups()).getResultList();


        DefaultListModel dlm = new DefaultListModel();
        Iterator<Object[]> it = results.iterator();
        Object[] result;
        DriverModel dm;
        while (it.hasNext()) {
            result = it.next();
            dm = new DriverModel();
            BigInteger bi = (BigInteger) result[0];
            dm.setDriverId(bi.longValue());
            dm.setDriverFirstName((String) result[1]);
            dm.setDriverSecondName((String) result[2]);
            bi = (BigInteger) result[3];
            dm.setModelId(bi.longValue());
            dm.setModelName((String) result[4]);
            dlm.addElement(dm);
        }

        return dlm;

    }

    public static Hashtable<String, RaceResult> createInitialResultsTable(Long raceId, RaceSelection sel, EntityManager em) {
        Query q = em.createNamedQuery("GrouppedDrivers.selectByRound");
        List<GrouppedDrivers> list = q.setParameter("race_id", raceId).setParameter("category_id", sel.getCategory().getId()).setParameter("groups", sel.getGroups()).getResultList();

        Hashtable<String, RaceResult> resultsHash = new Hashtable<String, RaceResult>();
        int lastPosition = 0;
        Iterator<GrouppedDrivers> it = list.iterator();
        RaceResult r;
        GrouppedDrivers g;
        String racersIdentificatioId;
        while (it.hasNext()) {
            g = it.next();
            r = new RaceResult(raceId, sel.getCategory().getId(), sel.getGroups(), sel.getHeatsOrFinals());
            r.setStartingPosition(g.getStartingPosition());
            lastPosition = g.getStartingPosition();

            racersIdentificatioId = DBHelper.getModelsIdentId(g.getModelId(), em);
            String driverModelName = DBHelper.getDriverModelName(racersIdentificatioId, sel.getCategory().getId(), em);
            r.setDriverName(driverModelName);

            r.setCompleteRoundsAchieved(0);
            resultsHash.put(racersIdentificatioId, r);
        }
        lastPosition++;
        return resultsHash;
    }

    public static Category getCategoryById(Long id, EntityManager entityManager) {
        Query q = entityManager.createNamedQuery("Category.findById");
        List<Category> catList = q.setParameter("id", id).getResultList();
        if (catList == null) {
            return new Category();
        }
        if (catList.size() == 0) {
            return new Category();
        }
        return catList.get(0);
    }

    public static Vector getAllCategories(EntityManager entityManager) {
        Query q = entityManager.createNamedQuery("Category.all");
        List<Category> catList = q.getResultList();
        return new Vector(catList);
    }

    public static String getDriverModelName(String identificationId, Long currentCategoryId, EntityManager em) {
        Query q = em.createNamedQuery("Model.findByIdentificationIdAndCategoryId");
        List<Model> models = q.setParameter("identification_id", identificationId).setParameter("category_id", currentCategoryId).getResultList();
        if (models.size() == 0) {
            return null;
        }
        Model m = models.get(0);
        q = em.createNamedQuery("Driver.findById");
        List<Driver> drivers = q.setParameter("id", m.getDriverId()).getResultList();

        if (drivers.size() == 0) {
            return null;
        }
        Driver d = drivers.get(0);

        return d.getFirstName() + " " + d.getSecondName() + " /" + m.getModelName() + "(" + m.getIdentificationId() + ")";

    }

    public static String getModelsIdentId(Long modelId, EntityManager em) {
        Query q = em.createNamedQuery("Model.findById");
        List<Model> list = q.setParameter("id", modelId).getResultList();
        if (list.size() != 0) {
            return list.get(0).getIdentificationId();
        } else {
            return "";
        }
    }

    public static List<RaceResult> getResultsForRound(EntityManager e, Long raceId,
            RaceSelection sel) {
  
        Query q = e.createNamedQuery("RaceResult.findByRound");
        
        q.setHint("org.hibernate.cacheable", false);
        

/*        Query q = e.createQuery("SELECT r FROM Result r WHERE " +
                "r.raceId = :race_id and r.categoryId = :category_id " +
                "and r.groups = :groups " +
                "and r.finalNumber = :final_number and r.heatNumber = :heat_number");
  */            

        List<RaceResult> results = q.setParameter("race_id", raceId)
                .setParameter("category_id", sel.getCategory().getId())
                .setParameter("groups", sel.getGroups())
                .setParameter("heat_number", sel.getHeatsOrFinals()[0])
                .setParameter("final_number", sel.getHeatsOrFinals()[1]).getResultList();

        return results;
    }

    public static List<DriverModel> getSelectedRacersForRaceGroupCat(Long raceId,
            RaceSelection sel, EntityManager em) {
        Query q = em.createNativeQuery("SELECT d.ID driver_id, d.FIRST_NAME, d.SECOND_NAME," +
                "m.ID model_id, m.MODEL_NAME " +
                "FROM groupped_drivers g, driver d, model m WHERE " +
                "d.id = g.driver_id and m.id = g.model_id and g.race_id = :race_id and " +
                "g.category_id = :category_id and g.groups = :groups  " +
                "ORDER BY g.STARTING_POSITION");
        List<Object[]> qResult = q.setParameter("race_id", raceId).setParameter("category_id", sel.getCategory().getId()).setParameter("groups", sel.getGroups()).getResultList();

        Iterator<Object[]> it = qResult.iterator();
        Object[] row;
        List<DriverModel> result = new ArrayList<DriverModel>();
        DriverModel dm;
        while (it.hasNext()) {
            row = it.next();
            dm = new DriverModel();
            BigInteger bi = (BigInteger) row[0];
            dm.setDriverId(bi.longValue());
            dm.setDriverFirstName((String) row[1]);
            dm.setDriverSecondName((String) row[2]);
            BigInteger bi2 = (BigInteger) row[3];
            dm.setModelId(bi2.longValue());
            dm.setModelName((String) row[4]);
            result.add(dm);
        }
        return result;
    }

    public static void saveResults(EntityManager entityManager, Long raceId, Long categoryId, String groups, int[] heatOrRound, Hashtable resultsHash) {


        Query q = entityManager.createNativeQuery("DELETE FROM RACE_RESULT r WHERE r.RACE_ID= :raceId and r.CATEGORY_ID= :categoryId and r.GROUPS = :groups and r.FINAL_NUMBER = :finalNumber and r.HEAT_NUMBER = :heatNumber");

        synchronized (raceId) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }


            entityManager.getTransaction().begin();
            q.setParameter("raceId", raceId).setParameter("categoryId", categoryId)
                    .setParameter("groups", groups).setParameter("finalNumber", heatOrRound[1])
                    .setParameter("heatNumber", heatOrRound[0]).executeUpdate();


            Enumeration<RaceResult> e = resultsHash.elements();

            while (e.hasMoreElements()) {
                entityManager.persist(e.nextElement());
            }

            entityManager.getTransaction().commit();
        }
    }

    public static void storeDriverModelList(Long raceId, RaceSelection sel,
            DefaultListModel toStore, EntityManager em) {

        GrouppedDrivers gd;
        DriverModel dm;

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        Query q = em.createNamedQuery("GrouppedDrivers.deleteForRace");
        q.setParameter("race_id", raceId).setParameter("category_id", sel.getCategory().getId()).setParameter("groups", sel.getGroups()).executeUpdate();

        for (int i = 0; i < toStore.getSize(); i++) {
            dm = (DriverModel) toStore.getElementAt(i);
            gd = new GrouppedDrivers();
            gd.setRaceId(raceId);
            gd.setCategoryId(sel.getCategory().getId());
            gd.setGroups(sel.getGroups());
            gd.setDriverId(dm.getDriverId());
            gd.setModelId(dm.getModelId());
            gd.setStartingPosition(i + 1);
            em.persist(gd);
        }


    }

    public static void updateResults(String orgValue, String value, Long rowId, int column, 
            long raceId, RaceSelection race, EntityManager e) {

        String query = "UPDATE race_result set ";
        String updatedField = "";
        switch (column) {
            case 0:
                updatedField = "driver_name";
                break;
            case 1:
                updatedField= "position";
                break;
            case 2:
                updatedField="complete_rounds_achieved";
                break;
            case 3:
                updatedField="starting_position";
                break;
            case 4:
                updatedField="best_round_time";
                break;
        }
        
        query += updatedField + "=:value where " + updatedField + "=:orgValue AND " +
                "race_id=:race_id AND category_id=:category_id AND final_number=" +
                ":final_number AND groups=:groups AND heat_number=:heat_number"
                + " AND id=:rowid";

        Query q = e.createNativeQuery(query);
            
        q.setParameter("value", value).setParameter("orgValue", orgValue)
                .setParameter("race_id", raceId).setParameter("category_id", race.getCategory().getId())
                .setParameter("final_number", race.getHeatsOrFinals()[1])
                .setParameter("groups", race.getGroups())
                .setParameter("heat_number", race.getHeatsOrFinals()[0])
                .setParameter("rowid",rowId).executeUpdate();


    }
}
