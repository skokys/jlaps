/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.utils.audio;

import java.io.IOException;
import java.io.File;

import java.lang.Override;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.AudioFileFormat;

public class AudioManagerRecorder extends Thread {

    private TargetDataLine m_line;
    private AudioFileFormat.Type m_targetType;
    private AudioInputStream m_audioInputStream;
    private File m_outputFile;

    public AudioManagerRecorder() {
        super();

    }

    public void startRecording() {
        m_line.start();
        super.start();
    }

    public void stopRecording() {
        if ( m_line != null ) {
            m_line.stop();
            m_line.close();
        }
    }

    @Override
    public void run() {
        try {
            AudioSystem.write(
                    m_audioInputStream,
                    m_targetType,
                    m_outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String prepareRecording(String filename) {

        File dir = new File("voices");
        if (!dir.exists()) {
            dir.mkdir();
        }
        String strFilename = "voices/" + filename + ".wav";
        File outputFile = new File(strFilename);

        /* For simplicity, the audio data format used for recording
        is hardcoded here. We use PCM 44.1 kHz, 16 bit signed,
        stereo.
         */
        AudioFormat audioFormat = new AudioFormat(
                AudioFormat.Encoding.PCM_SIGNED,
                44100.0F, 16, 2, 4, 44100.0F, false);

        /* Now, we are trying to get a TargetDataLine. The
        TargetDataLine is used later to read audio data from it.
        If requesting the line was successful, we are opening
        it (important!).
         */
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, audioFormat);
        TargetDataLine targetDataLine = null;
        try {
            targetDataLine = (TargetDataLine) AudioSystem.getLine(info);
            targetDataLine.open(audioFormat);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
            return e.getMessage();
        }

        AudioFileFormat.Type targetType = AudioFileFormat.Type.WAVE;

        m_line = targetDataLine;
        m_audioInputStream = new AudioInputStream(targetDataLine);
        m_targetType = targetType;
        m_outputFile = outputFile;

        return null;
    }
    private static final int EXTERNAL_BUFFER_SIZE = 128000;
}
