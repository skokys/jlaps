/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.utils.audio;

import java.io.IOException;
import java.io.File;

import javax.sound.sampled.DataLine;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class AudioManagerPlayer extends Thread {

    private String toPlay;

    public AudioManagerPlayer(String toPlay) {
        super();
        this.toPlay = toPlay;
    }

    public boolean canYouSay(String text) {
        File f = new File("voices/" + text + ".wav");
        return f.exists();

    }

    public void run() {
        play(toPlay);

    }
    private static final int EXTERNAL_BUFFER_SIZE = 128000;

    public synchronized String play(String filename) {

        /*
        Now, that we're shure there is an argument, we
        take it as the filename of the soundfile
        we want to play.
         */
        String strFilename = "voices/" + filename + ".wav";

        File soundFile = new File(strFilename);

        /*
        We have to read in the sound file.
         */
        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(soundFile);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }


        AudioFormat audioFormat = audioInputStream.getFormat();

        SourceDataLine line = null;
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
        try {
            line = (SourceDataLine) AudioSystem.getLine(info);

            line.open(audioFormat);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }

        line.start();


        int nBytesRead = 0;
        byte[] abData = new byte[EXTERNAL_BUFFER_SIZE];
        while (nBytesRead != -1) {
            try {
                nBytesRead = audioInputStream.read(abData, 0, abData.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (nBytesRead >= 0) {
                int nBytesWritten = line.write(abData, 0, nBytesRead);
            }
        }

        line.drain();

        line.close();

        return null;

    }
}
