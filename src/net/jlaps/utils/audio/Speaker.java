/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.utils.audio;

import com.sun.speech.freetts.OutputQueue;
import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;
import java.util.Vector;
import net.jlaps.JLapsView;

/**
 *
 * @author skoky
 */
public class Speaker extends Thread {

    private Voice helloVoice;
    private OutputQueue q;
    String lastTextToSay;
    String lastVoice;
    JLapsView view;
    private boolean speakingInProgress = false;

    public Speaker(String textToSay, JLapsView v, String voice) {
        super();
        start(textToSay,v,voice);

    }

    public Speaker(JLapsView v, String voice) {
        super();
        start("", v,  voice);
        
    }
    
    private void start(String textToSay, JLapsView v, String voice) {
        this.lastTextToSay = textToSay;
        this.lastVoice = voice;
        this.view = v;
        if ( voice != null ) 
            init(voice);        
    }

    public void setLastTextToSay(String lastTextToSay) {
        this.lastTextToSay = lastTextToSay;
    }
    public boolean speakingInProgress() {
        return speakingInProgress;
    }

 
    private void init(String voice) {
        listAllVoices();
        VoiceManager voiceManager = VoiceManager.getInstance();
        helloVoice = voiceManager.getVoice(voice);
        

                
//        helloVoice = new CMUDiphoneVoice();
        
        

    }

    public Vector listAllVoices() {
        VoiceManager voiceManager = VoiceManager.getInstance();
        Voice[] voices = voiceManager.getVoices();
        Vector<String> voicesList = new Vector<String>();
        for (int i = 0; i < voices.length; i++) {
            voicesList.add(voices[i].getName());
        }
        return voicesList;
    }

    public void setTextToSay(String text) {
        this.lastTextToSay = text;
    }

    public void say(String textToSay) {

        if (textToSay == null) {
            return;
        }
        

        speakingInProgress = true;
        System.out.println("Going to say:" + textToSay);
        helloVoice.allocate();
        helloVoice.speak(textToSay);
        helloVoice.deallocate();
        speakingInProgress = false;

    }

    public void run() {

        if (lastTextToSay == null) {
            return;
        }
        say(lastTextToSay);
    }
}
