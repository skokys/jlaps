/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.utils;

import java.util.Iterator;
import java.util.List;
import javax.swing.table.TableColumnModel;
import net.jlaps.entity.Model;

/**
 *
 * @author lskokan
 */
public class JLapsUtils {

    public static Object[][] convertModelsToArray(List<Model> r) {
        Object[][] result = new String[r.size()][5];
        Iterator<Model> it = r.iterator();
        Model m;

        for (int i = 0; i < r.size(); i++) {
            m = it.next();
            result[i][0] = m.getModelName();
            result[i][1] = m.getFrequency1();
            result[i][2] = m.getIdentificationType();
            result[i][3] = m.getIdentificationId();
            result[i][4] = m.getCategory();
        }
        return result;
    }

    public static String[] convertTableColumnsToArray(TableColumnModel columnModel) {
        
        String[] columns = new String[columnModel.getColumnCount()];
        for (int i=0;i<columnModel.getColumnCount();i++)
            columns[i]=columnModel.getColumn(i).getHeaderValue().toString();        
        return columns;
    }

}
