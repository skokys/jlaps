/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.utils.tableutils;

import java.awt.Color;
import java.awt.Component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
 
/**
 *
 * @author skoky
 */
   public class DateEditor extends DefaultCellEditor {

        String patternE = "dd.MM.yyyy";
        SimpleDateFormat formatterE = new SimpleDateFormat(patternE);

        public DateEditor() {
            super(new JTextField());
        }

        @Override
        public boolean stopCellEditing() {
            String value = ((JTextField) getComponent()).getText();
            if (!value.equals("")) {
                try {
                    formatterE.parse(value);
                } catch (ParseException e) {
                    ((JComponent) getComponent()).setBorder(new LineBorder(Color.red));
                    return false;
                }
            }
            return super.stopCellEditing();
        }


        @Override
        public Component getTableCellEditorComponent(final JTable table, final Object value,
                final boolean isSelected, final int row, final int column) {
            JTextField tf = ((JTextField) getComponent());
            tf.setBorder(new LineBorder(Color.black));
            try {
                tf.setText(formatterE.format(value));
            } catch (Exception e) {
                tf.setText("");
            }
            return tf;
        }

        @Override
        public Object getCellEditorValue() {
            try {
                Date value = formatterE.parse(((JTextField) getComponent()).getText());
                return value;
            } catch (ParseException ex) {
                return null;
            }
        }
            
   }