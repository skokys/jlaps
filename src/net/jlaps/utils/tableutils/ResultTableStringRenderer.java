/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.utils.tableutils;

import java.awt.Color;
import java.awt.Component;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author lskokan
 */
public class ResultTableStringRenderer extends DefaultTableCellRenderer {

    Vector<Integer> grayList;

    public ResultTableStringRenderer() {
        super();
        grayList = new Vector<Integer>();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (grayList.contains(row)) {
            setBackground(Color.LIGHT_GRAY);
        } else {
            if (isSelected) {
                setBackground(new Color(85, 152, 215));
            } else {
                setBackground(Color.WHITE);
            }
        }
        return c;
    }

    public void setGrayRow(int row) {
        if (!grayList.contains(row)) {
            grayList.add(row);
        }
    }
    public void cleanGrayList() {
        grayList.clear();
    }
    
}
