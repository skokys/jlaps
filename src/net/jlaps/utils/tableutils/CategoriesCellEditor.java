/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.utils.tableutils;

import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import net.jlaps.*;


/**
 *
 * @author skoky
 */
public class CategoriesCellEditor extends AbstractCellEditor implements TableCellEditor {

    private JComboBox jcb;
    public CategoriesCellEditor() {
        super();
        jcb = new JComboBox();
        jcb.addItem("abc");
        jcb.addItem("def");
    }
    


    public Object getCellEditorValue() {
        return jcb.getSelectedItem();
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return jcb;
    }

}
