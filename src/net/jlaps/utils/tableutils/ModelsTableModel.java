/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.utils.tableutils;

import net.jlaps.utils.*;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import net.jlaps.entity.Category;
import net.jlaps.entity.Model;
import net.jlaps.utils.DBHelper;

/**
 *
 * @author lskokan
 */
public class ModelsTableModel extends AbstractTableModel {

    private Hashtable<Integer, String> columnNames = new Hashtable<Integer, String>();

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 4) {
            return Long.class;
        } else {
            return String.class;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames.get(column);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        super.setValueAt(aValue, rowIndex, columnIndex);
        Model m = models.get(rowIndex);

        if (m != null) {
            if (columnIndex == 0) {
                m.setModelName((String) aValue);
            } else if (columnIndex == 1) {
                m.setFrequency1((String) aValue);
            } else if (columnIndex == 2) {
                m.setIdentificationType((String) aValue);
            } else if (columnIndex == 3) {
                m.setIdentificationId((String) aValue);
            } else if (columnIndex == 4) {
                Category c = (Category) aValue;
                m.setCategory((Long) c.getId());
            }
        }

        fireTableChanged(new TableModelEvent(this));
    }
    private Long driverId;
    private EntityManager em;
    private Hashtable<Integer, Model> models;

    public ModelsTableModel() {
        driverId = null;
        refreshModelsList();
        initColumns();
    }

    public ModelsTableModel(Long driverId, EntityManager em) {
        super();
        this.driverId = driverId;
        this.em = em;
        refreshModelsList();
        initColumns();
        fireTableChanged(new TableModelEvent(this));
    }

    public int getRowCount() {
        return models.size();
    }

    public int getColumnCount() {
        return 5;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Model m = models.get(rowIndex);
        if (m == null) {
            return null;
        }
        Object value = null;
        if (columnIndex == 0) {
            value = m.getModelName();
        } else if (columnIndex == 1) {
            value = m.getFrequency1();
        } else if (columnIndex == 2) {
            value = m.getIdentificationType();
        } else if (columnIndex == 3) {
            value = m.getIdentificationId();
        } else if (columnIndex == 4) {
            if ( m.getCategory() == null )
                return null;
            Long catId = m.getCategory();
            value = DBHelper.getCategoryById(catId, em); 
            return value;
        }
        if (value == null) {
            value = null;
        }
        return value;
    }

    public void addRecord(Model m) {
        int size = models.size();
        models.put(size, m);
        em.persist(m);
        fireTableChanged(new TableModelEvent(this));
    }

    public void removeRecord(int i) {
        Model m = models.get(i);
        em.remove(m);
        models.remove(i);
        for (int x = i + 1; x < models.size(); x++) {
            m = models.get(x);
            models.put(x - 1, m);
        }
        fireTableChanged(new TableModelEvent(this));
    }

    private void createNewList() {
        models = new Hashtable<Integer, Model>();
    }

    private void initColumns() {
        columnNames.put(0, "Model name");
        columnNames.put(1, "Frequency 1");
        columnNames.put(2, "Id type");
        columnNames.put(3, "ID");
        columnNames.put(4, "Category");
    }

    private void refreshModelsList() {
        if (driverId == null) {
            createNewList();
            return;
        }
        Query q = em.createNamedQuery("Model.findByDriverId");
        List<Model> list = q.setParameter("driverId", driverId).getResultList();
        models = new Hashtable<Integer, Model>();

        Iterator<Model> it = list.iterator();
        for (int i = 0; i < list.size(); i++) {
            models.put(i, it.next());
        }

    }
}
