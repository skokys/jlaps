/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.utils.tableutils;

import java.util.ArrayList;
import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import net.jlaps.entity.Category;

/**
 *
 * @author lskokan
 */
public class CategoryListModel implements ListModel {

    private List<Category>  categories = new ArrayList();
    
    public CategoryListModel(List<Category> cats) {
        this.categories = cats;
    }
    public int getSize() {
        return categories.size();
    }

    public Object getElementAt(int arg0) {
        return categories.get(arg0);
    }

    public void addListDataListener(ListDataListener arg0) {

    }

    public void removeListDataListener(ListDataListener arg0) {

    }
    
    public List<Category> getCategories() {
        return categories;
    }

}
