/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.utils.tableutils;

import net.jlaps.entity.Category;

/**
 *
 * @author lskokan
 */
public class RaceSelection {

    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    private int[] heatsOrFinals;

    public int[] getHeatsOrFinals() {
        return heatsOrFinals;
    }

    public void setHeatsOrFinals(int[] heatsOrFinals) {
        this.heatsOrFinals = heatsOrFinals;
    }
    private String groups;

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }



    public RaceSelection() {
    }
    
    @Override
    public String toString() {
        if ( category != null )
            return category.getName() + ":" + groups + ":" + heatsOrFinals[0] + "?" + heatsOrFinals[1];
        else
            return "Invalid selection";
    }
}
