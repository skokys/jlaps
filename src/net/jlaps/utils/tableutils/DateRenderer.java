/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.utils.tableutils;

import java.text.DateFormat;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author skoky
 */
public class DateRenderer extends DefaultTableCellRenderer {

        DateFormat formatter;

        public DateRenderer() {
            super();
        }

        public void setValue(Object value) {
            if (formatter == null) {
                formatter = DateFormat.getDateInstance(DateFormat.SHORT);

            }
            setText((value == null) ? "" : formatter.format(value));
        }
    }