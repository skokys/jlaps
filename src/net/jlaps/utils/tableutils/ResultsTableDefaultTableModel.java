/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.utils.tableutils;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author lskokan
 */
public class ResultsTableDefaultTableModel extends DefaultTableModel {

    private Object orgValue;
    public ResultsTableDefaultTableModel() {
        super();
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        orgValue = this.getValueAt(row, column);
        super.setValueAt(aValue, row, column);
    }
    
    public Object getOrgValue() {
        return orgValue;
    } 
    
    

    
}
