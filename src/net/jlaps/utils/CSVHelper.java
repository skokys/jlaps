/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.jlaps.utils.CSVHelper.ImportedDataObject; 

/**
 *
 * @author lskokan
 */
public class CSVHelper {

    public static Vector<ImportedDataObject> parseFile(File file) {
        BufferedReader in = null;
        Vector<ImportedDataObject> results = new Vector<ImportedDataObject>();
        try {
            in = new BufferedReader(new FileReader(file));

            while (true) {
                String line = in.readLine();
                if (line != null) {
                    ImportedDataObject ido = parseLine(line);
                    if ( ido != null )
                        results.add(ido);
                } else
                    break;
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println("File does not exists " + file.getAbsolutePath() + " error:" + fnfe);
            return null;
        } catch (IOException ioe) {
            System.err.println("Error reading file " + file.getAbsolutePath() + " error:" + ioe);
            return null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(CSVHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return results;
    }

    private static CSVHelper.ImportedDataObject parseLine(String line) {
        if (line == null) {
            return null;
        }
        if (line.length() == 0) {
            return null;
        }

        ImportedDataObject ido = new ImportedDataObject();;
       try {
        StringTokenizer st1 = new StringTokenizer(line, ",");
        
        StringTokenizer st2 = new StringTokenizer(st1.nextToken(), " ");
        ido.setDriverFirstName(st2.nextToken());
        ido.setDriverSurname(st2.nextToken());
        ido.setCategoryNumber(st1.nextToken());
        st1.nextToken();
        //st1.nextToken();
        ido.setAmbId(st1.nextToken());
       } catch (Exception e ) {
           System.err.println("Error parsing row:" + line + " " + e.getMessage());
       }
        return ido;
    }

    public static class ImportedDataObject {

        private String driverFirstName;

        public String getAmbId() {
            return ambId;
        }

        public void setAmbId(String ambId) {
            this.ambId = ambId;
        }

        public String getCategoryNumber() {
            return categoryNumber;
        }

        public void setCategoryNumber(String categoryNumber) {
            this.categoryNumber = categoryNumber;
        }

        public String getDriverFirstName() {
            return driverFirstName;
        }

        public void setDriverFirstName(String driverFirstName) {
            this.driverFirstName = driverFirstName;
        }

        public String getDriverSurname() {
            return driverSurname;
        }

        public void setDriverSurname(String driverSurname) {
            this.driverSurname = driverSurname;
        }
        private String driverSurname;
        private String categoryNumber;
        private String ambId;
        
        @Override
        public String toString() {
            return driverFirstName + " " + driverSurname + " category:" + 
                    categoryNumber + " AMB id:" + ambId;
        }
    }
}
