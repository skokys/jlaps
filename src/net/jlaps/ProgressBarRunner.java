/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;

/**
 *
 * @author lskokan
 */
class ProgressBarRunner extends Thread {

    private int duractionSecs;
    private boolean stopNow = false;
    private JProgressBar bar;

    public ProgressBarRunner(int duractionSecs, JProgressBar bar) {
        this.duractionSecs = duractionSecs;
        this.bar = bar;
        bar.setMinimum(0);
        bar.setMaximum(duractionSecs);
        bar.setVisible(true);
        System.out.println("Progress running for " + duractionSecs + "secs");
    }

    @Override
    public void run() {
        super.run();
        stopNow = false;

        bar.setEnabled(true);
        for (int i = 0; (!stopNow && i < duractionSecs); i++) {
            // System.out.println("Progress update " + i + "secs");
            {
                bar.setValue(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ProgressBarRunner.class.getName()).log(Level.SEVERE, null, ex);
                }
                bar.setValue(bar.getMaximum());
                bar.setEnabled(false);
            }
        }
        
    }

    public void stopNow() {
        this.stopNow = true;
        bar.setVisible(false);
    }
}
