package net.jlaps;

import java.awt.event.ActionEvent;
import java.util.EventObject;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;


/**
 * The main class of the application. Mainly the screen with related logic
 */
public class JLapsApp extends SingleFrameApplication {



    
    /**
     * At startup create and show the main frame of the application. assigns
     * close dialog to the exit action
     */
    @Override protected void startup() {
        ExitListener maybeExit = new ExitListener() {
            public boolean canExit(EventObject e) {
                int option = JOptionPane.showConfirmDialog(null, "Really Exit?");
                return option == JOptionPane.YES_OPTION;
            }
            public void willExit(EventObject e) { }
        };        
        
        addExitListener(maybeExit);
            JButton button = new JButton();
            button.setName("button");
            button.setAction(new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    exit(e);
                }
            });        
        
        show(new JLapsView(this));
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of JLapsApp
     */
    public static JLapsApp getApplication() {
        return Application.getInstance(JLapsApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) {

        JLapsConfig.generateDefaultSpeechProperties();
        launch(JLapsApp.class, args);
        
    }
}
