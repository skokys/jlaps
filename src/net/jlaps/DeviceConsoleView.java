/*
 * This is a device console view 
 *
 * Created on February 20, 2008, 8:47 PM
 */

package net.jlaps;

import java.awt.Image;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.DefaultListModel;


/**
 * This is a frame providing the device console - o see binary data comming
 * from device. This is a great tool for debugging
 * @see net.jlaps.DeviceConsole
 * @author  lskokan
 */
public class DeviceConsoleView extends javax.swing.JFrame implements DeviceConsole {
    
    private DefaultListModel listModel;
    private  SimpleDateFormat formatter;
    
    /** Creates new form DeviceConsoleView */
    public DeviceConsoleView(Image ii) {
        super();
        initComponents();
        setIconImage(ii);
        setName("JLaps AMB Console");
        listModel = new DefaultListModel();
        list.setModel(listModel);
        list.setEnabled(true);

        formatter =
                new SimpleDateFormat("HH:mm:ss.ms");      
        
    }
    

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        list = new javax.swing.JList();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(net.jlaps.JLapsApp.class).getContext().getResourceMap(DeviceConsoleView.class);
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setForeground(resourceMap.getColor("Form.foreground")); // NOI18N
        setName("Form"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        list.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        list.setName("list"); // NOI18N
        list.setVisibleRowCount(100);
        jScrollPane1.setViewportView(list);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList list;
    // End of variables declaration//GEN-END:variables

    /**
     * adds event to embeded scroll pane
     * @param event
     * @param dateTime
     */
    public void addEvent(String event, Date dateTime) {
        
        listModel.addElement(formatter.format(dateTime) + " : " + event);
        if ( listModel.getSize() > 20)
            listModel.remove(0);
        list.setSelectedIndex(listModel.getSize()-1);
        
    }
    
}
