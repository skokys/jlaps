package net.jlaps;

import java.util.logging.Level;
import net.jlaps.calculator.ResultsHelper;
import net.jlaps.utils.tableutils.RaceSelection;
import net.jlaps.utils.audio.Speaker;
import net.jlaps.utils.audio.AudioManagerRecorder;
import net.jlaps.utils.audio.AudioManagerPlayer;
import net.jlaps.DeviceConsole;
import net.jlaps.JLapsConfig;
import net.jlaps.JLapsSuperView;
import net.jlaps.RaceSetupView;
import net.jlaps.utils.tableutils.ResultsTableDefaultTableModel;
import net.jlaps.ViewScreen;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import net.jlaps.calculator.SimpleResultsCalculator;
import net.jlaps.calculator.SimpleRaceRunner;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import org.jdesktop.application.Action;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import javax.persistence.Query;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.RowSorter;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import net.jlaps.entity.Category;
import net.jlaps.entity.DriverModel;
import net.jlaps.entity.Race;
import net.jlaps.entity.RaceResult;
import net.jlaps.entity.helpers.RaceHelper;
import net.jlaps.entity.helpers.ResultsComparator;
import net.jlaps.readers.AMBReaderRc3;
import net.jlaps.reports.JasperReportViewer;
import net.jlaps.simulator.SimulatorThread;
import net.jlaps.utils.*;
import net.jlaps.utils.audio.JSAPI;
import net.jlaps.utils.tableutils.ResultTableStringRenderer;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.jdesktop.beansbinding.AbstractBindingListener;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.PropertyStateEvent;

/**
 * The application's main frame and related functionality
 */
public class JLapsView extends JLapsSuperView implements ViewScreen {

    private Logger log = Logger.getLogger(this.getClass().toString());
    private Hashtable<String, RaceResult> initialResultHash;
    private boolean isLapPostTime;
    private JasperReportViewer reportViewer;
    private RaceSetupView raceSetupView;
    private SingleFrameApplication app;
    private DeviceConsole consoleScreen;
    // current selections
//    private Category currentCategory;
//    private int[] currentHeatOrFinal;
//    private String currentGroups;

    // button action listeners
    private JLapsView.CategoryAL buttonCategoriesActionListener = new JLapsView.CategoryAL();
    private JLapsView.HeatsAL buttonHeatsActionListener = new JLapsView.HeatsAL();
    private JLapsView.FinalsAL buttonFinalsActionListener = new JLapsView.FinalsAL();
    private JLapsView.GroupsAL buttonGroupsActionListener = new JLapsView.GroupsAL();
    private List<RaceResult> resultsTableList;
    private SimpleResultsCalculator resultsCalculator;
    private TableModelListener resultTableChangeListener;
    private RaceSelection currentSelection;
    private boolean raceRunning;
    private SimulatorThread simulator;

    public boolean isLapPostTime() {
        return isLapPostTime;
    }

    public void setIsPostTime() {
        log.info("Setting posttime");
        this.isLapPostTime = true;
    }

    @Override
    public void refreshResults(String identification, Long timestamp) {
//        Runnable r = new SimpleResultsCalculator(identification, timestamp,todaysRace.getId(),getCurrentCategoryId(),getCurrentHeatOrRound(),this,entityManager);
        if (resultsCalculator != null) {
            resultsCalculator.setEventToProcess(identification, timestamp);
            resultsCalculator.run();
        }
    }

    private void createRaceCategoriesButtons(Hashtable<Category, List> catGroups,
            Race r) {

        ButtonGroup catGroup = new ButtonGroup();
        JPanel catPanel = new JPanel(new GridLayout(catGroups.size(), 1));

        if (catGroups.size() == 0) {
            return;
        }
        Enumeration<Category> it = catGroups.keys();
        Category c = new Category();
        for (int i = 0; i < catGroups.size(); i++) {
            c = it.nextElement();
            JRadioButton button = new JRadioButton(c.getName());
            button.setActionCommand(new Long(c.getId()).toString());
            button.addActionListener(buttonCategoriesActionListener);
            setButtonFont(button);
            if (i == 0) {
                button.setSelected(true);
                currentSelection.setCategory(c);
            }
            catPanel.add(button);
            catGroup.add(button);
        }

        dynPanel.removeAll();
        dynPanel.setLayout(new GridLayout(1, 4));
        dynPanel.add(catPanel);
        dynPanel.repaint();
        dynPanel.updateUI();

        createHeatAndFinalsButtons(currentSelection.getCategory());
        recreateGroupsButton(todaysRace);


    }

    private void createHeatAndFinalsButtons(Category c) {
        ButtonGroup heatFinalGroup = new ButtonGroup();

        JPanel heatPanel = new JPanel(new GridLayout(c.getHeats(), 1));
//      currentHeatOrFinal = new int[2];

        for (int x = 1; x <= c.getHeats(); x++) {
            String name = msg("heat") + " " + x;
            JRadioButton button = new JRadioButton(name);
            button.setActionCommand(new Integer(x).toString());
            button.addActionListener(buttonHeatsActionListener);
            setButtonFont(button);
            if (x == 1) {
                button.setSelected(true);
                //              currentHeatOrFinal[0]=1;
                currentSelection.setHeatsOrFinals(new int[]{1, 0});
            }
            heatPanel.add(button);
            heatFinalGroup.add(button);
        }



        JPanel finalPanel = new JPanel(new GridLayout(c.getFinals(), 1));
        for (int x = 1; x <= c.getFinals(); x++) {
            String name = msg("final") + " " + x;
            JRadioButton button = new JRadioButton(name);
            button.setActionCommand(new Integer(x).toString());
            button.addActionListener(buttonFinalsActionListener);
            setButtonFont(button);
            finalPanel.add(button);
            heatFinalGroup.add(button);
        }


        dynPanel.add(heatPanel);
        dynPanel.add(finalPanel);
        dynPanel.repaint();
        dynPanel.updateUI();

    }

    private void disableDuringRace(boolean b) {
        dynPanel.setEnabled(!b);
        Component[] c = dynPanel.getComponents();
        for (int i = 0; i < c.length; i++) {
            JPanel p = (JPanel) c[i];
            Component[] cc = p.getComponents();
            for (int y = 0; y < cc.length; y++) {
                cc[y].setEnabled(!b);
            }
        }
        jButton8.setEnabled(!b);
        jButton9.setEnabled(!b);
        jButton13.setEnabled(!b);
        jButton5.setEnabled(!b);
        refreshEventsButton.setEnabled(!b);
        jButton1.setEnabled(!b);

    }

    void setButtonFont(
            JRadioButton button) {
        Font currentFont = mainPanel.getFont();
        int size = currentFont.getSize();
        button.setFont(currentFont.deriveFont(size));

    }

    private String milisToTime(Long finishedTime) {
        if (finishedTime == null) {
            return "0";
        }
        Long minutes = finishedTime / 60000;
        Long seconds = (finishedTime % 60000) / 1000;
        Long msecs = finishedTime - (minutes * 60000 + seconds * 1000);
        return minutes + ":" + seconds + "." + msecs;
    }

    private class ResultTableListener implements TableModelListener {

        public ResultTableListener() {
        }

        public void tableChanged(TableModelEvent e) {
            ResultsTableDefaultTableModel dtm;
            if (e.getType() == TableModelEvent.UPDATE) {
                if (raceRunning) {
                    return;
                }
                dtm = (ResultsTableDefaultTableModel) e.getSource();
                String value = (String) dtm.getValueAt(e.getFirstRow(), e.getColumn());
                String orgValue = (String) dtm.getOrgValue();
                Long rowId = new Long((String) dtm.getValueAt(e.getFirstRow(), 6));
                log.info("Changing from:" + orgValue + " to " + value + " in row:" + e.getFirstRow());

                DBHelper.updateResults(orgValue, value, rowId, e.getColumn(), todaysRace.getId(), currentSelection, entityManager);

                setSaveNeeded(true);
            }
        }
    }

    private void enableResultTableEditing(boolean b) {
        if (resultTableChangeListener == null) {
            resultTableChangeListener = new ResultTableListener();
        }
        if (b) {
            resultTable.getModel().addTableModelListener(resultTableChangeListener);
            resultTable.setEnabled(true);
            raceRunning = false;
        } else {
            resultTable.getModel().removeTableModelListener(resultTableChangeListener);
            resultTable.setEnabled(false);
            raceRunning = true;
        }
    }

    private void recreateGroupsButton(Race r) {

        int numOfGroups = RaceHelper.getNumOfGroupsForRaceAndCat(todaysRace, currentSelection.getCategory().getId());

        if (numOfGroups == 0) {
            return;
        }

        ButtonGroup groupsGroup = new ButtonGroup();
        JPanel panel = null;

        if (numOfGroups == 1) {
            delGroupsButton.setEnabled(false);
        } else {
            delGroupsButton.setEnabled(true);
        }
        if (numOfGroups == 1 || numOfGroups == 2) {
            currentSelection.setGroups("A");

            panel = new JPanel(new GridLayout(numOfGroups, 1));
        } else if (numOfGroups > 2) {
            delGroupsButton.setEnabled(true);
            panel = new JPanel(new GridLayout(numOfGroups / 2, 2));
        }
        // panel = new JPanel(new GridLayout(numOfGroups / 3, 3));

        panel.removeAll();
        char groupName = 'A';
        for (int x = 0; x < numOfGroups; x++) {
            char currGroup = (char) (groupName + x);
            String name = new String((new char[]{currGroup}));
            JRadioButton button = new JRadioButton(name);
            button.setActionCommand(name);
            button.addActionListener(buttonGroupsActionListener);
            setButtonFont(button);
            if (x == 0) {
                button.setSelected(true);
                currentSelection.setGroups(name);
            }
            panel.add(button);
            groupsGroup.add(button);
        }

        if (dynPanel.getComponentCount() > 3) {
            dynPanel.remove(3);
        }
        dynPanel.add(panel);
        dynPanel.repaint();
        dynPanel.updateUI();

    }

    private void setRaceTitle(String msg) {
        TitledBorder tb = (TitledBorder) dynPanel.getBorder();
        tb.setTitle(msg);
    }

    /// speakers syntetiser instance
    private Speaker speaker;
    /// audio player and recorder
    private AudioManagerRecorder audioManagerRecorder;
    private AudioManagerPlayer audioManagerPlayer;
//    ResourceMap rm = getResourceMap();
//    static boolean counterRunning = false;
    /// AMB threads for device checking
    private Thread ambContinousThread;
    private AMBReaderRc3 ambReaderRef;
    private SimpleRaceRunner raceRunnerThread;
    private ProgressBarRunner pbr;

    /**
     * this to say race results. TBD
     */
    public void announceRaceResults() {
        say(msg("raceannouncement"));

    }


    /**
     * puts voices into teh configuration speech combo. Selects kevin16 as default
     * @param voices vector of strings
     */
    public void setSpeechVoicesCombo(Vector<String> voices) {
        voicesCombo.setModel(new DefaultComboBoxModel(voices));
        voicesCombo.setSelectedItem("kevin16");
    // voicesCombo.setSelectedIndex(0);
    }

    /**
     * provides results table model
     * @return
     */
    public TableModel getResultsTableModel() {
        return resultTable.getModel();
    }

    /**
     * refreshes events. requiries events table from DB
     */
    public void refreshEventsTable() {
        if (currentSelection.getCategory() == null) {
            return;
        }
        java.util.Collection data = eventQueryWithParams.setParameter("RACE_ID", todaysRace.getId()).setParameter("CATEGORY_ID", currentSelection.getCategory().getId()).setParameter("GROUPS", currentSelection.getGroups()).setParameter("HEAT_NUMBER", currentSelection.getHeatsOrFinals()[0]).setParameter("FINAL_NUMBER", currentSelection.getHeatsOrFinals()[1]).getResultList();
        eventList.clear();
        eventList.addAll(data);

    }

    /**
     * sets status line text
     * @param text
     */
    public void setStatusLineText(String text) {
        statusLine.setText(text);
    }

    @Action(enabledProperty = "saveNeeded")
    public Task saveEventAndResult() {
        return new SaveEventAndResultTask(getApplication());
    }

    private class SaveEventAndResultTask extends Task {

        SaveEventAndResultTask(org.jdesktop.application.Application app) {
            super(app);
        }

        @Override
        protected Void doInBackground() {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().commit();
                entityManager.getTransaction().begin();
                entityManager.flush();
            }
            return null;
        }

        @Override
        protected void finished() {
            setSaveNeeded(false);
        }
    }

    /**
     * creates empty result list. Later to be changed to add drivers from the category
     */
    private void createEmptyResultsList() {
        emptyResultList = new ArrayList();
        RaceResult r = new RaceResult();
        r.setDriverName("");
        emptyResultList.add(r);

    }

    /**
     * refreshes rsult table list from database
     * transfers table data into an array fitting to the table model
     */
    private void refreshResultTable() {

        if (currentSelection.getCategory() == null) {
            return;
        }
        log.info("Refreshing result for selection:" + currentSelection);
        enableResultTableEditing(false);

        /// refreshes results
        List<RaceResult> results = DBHelper.getResultsForRound(entityManager, todaysRace.getId(),
                currentSelection);
        resultsTableList = results;

        cleanGrayLines();
        Iterator<RaceResult> y = results.iterator();
        Vector<RaceResult> sortedResults = new Vector<RaceResult>();
        while (y.hasNext()) {
            sortedResults.add(y.next());
        }
        Collections.sort(sortedResults, new ResultsComparator(true));

        Enumeration<RaceResult> it = sortedResults.elements();

        String[][] tableData = new String[results.size()][7];
        RaceResult r;
        for (int i = 0; i < results.size(); i++) {
            r = (RaceResult) it.nextElement();
            if (r.getDriverName() != null) {
                tableData[i][0] = r.getDriverName();
            }
            if (r.getPosition() != null) {
                tableData[i][1] = r.getPosition().toString();
            }
            if (r.getCompleteRoundsAchieved() != null) {
                tableData[i][2] = r.getCompleteRoundsAchieved().toString();
            }
            if (r.getStartingPosition() != null) {
                tableData[i][3] = r.getStartingPosition().toString();
            }
            if (r.getBestRoundTime() != null) {
                tableData[i][4] = r.getBestRoundTime().toString();
            }
            if (r.getFinishedTime() != null) {
                tableData[i][5] = milisToTime(r.getFinishedTime());
            }
            tableData[i][6] = r.getId().toString();
        }

        updateResultsTable(tableData);

        enableResultTableEditing(true);

        // just a fake to hide ID from results table
        if (resultTable.getColumnCount() > 6) {
            resultTable.removeColumn(resultTable.getColumnModel().getColumn(6));
        }
    }

    /**
     * says a sentence in parameter. Check if the sentence already exists
     * as a recorded wav on the disk before.
     * Saying is done in a separated thread
     * @param text to say
     */
    private void say(String text) {

            if (text == null) {
                return;
            }
            // test if there is such a recording
            ComboBoxModel m = recordingsComboBox.getModel();
            boolean foundInLib = false;
            for (int i = 0; i < m.getSize(); i++) {
                if (text.equalsIgnoreCase((String) m.getElementAt(i))) {
                    foundInLib = true;
                    break;
                }
            }

            // tests if it is already recrded in lib
            if (foundInLib) {
                if (audioManagerPlayer == null) {
                    audioManagerPlayer = new AudioManagerPlayer(text);
                }
                if (audioManagerPlayer.canYouSay(text)) {
                    audioManagerPlayer = new AudioManagerPlayer(text);
                    ((Thread) audioManagerPlayer).start();
                    return;
                }
            }

            if (speaker == null) {
                speaker = new Speaker(this, (String) voicesCombo.getSelectedItem());
            }
            // no recording found, use voice syntetizer to pronounce
            speaker = new Speaker(text, this, (String) voicesCombo.getSelectedItem());
            speaker.start();
        

    }

    /**
     * selects todays race based on todays date - searching for races with todays race date
     */
    private void smartTodaysRaceSelector() {

        Race race = null, selectedRace = null;

        java.text.SimpleDateFormat sdf =
                new java.text.SimpleDateFormat("dd.MM.yyyy");


        String todayDate = sdf.format(new Date());
        String raceDate;

        raceList = raceQuery.getResultList();
        Iterator<Race> it = raceList.iterator();

        while (it.hasNext()) {
            race = it.next();

            raceDate = sdf.format(race.getRaceDate());

            if (todayDate.compareToIgnoreCase(raceDate) == 0) {
                selectedRace = race;
                break;
            }

        }


        if (selectedRace != null) {

            statusLine.setText(msg("Selecting") + selectedRace.getName() + " " + msg("astodaysrace"));
            setTodaysRaceFromOutside(selectedRace);
        }

        refreshEventsAndResultsAuto();

    }

    /**
     * tests undelying connection to AMB by checking last messgae delay received 
     * from AMB. If it more than 3 sec, it expectd the AMB to be disconnected
     * Sets text in the AMB status line in bottom right hand corner
     * @return true - AMB connected, false - disconnected
     */
    private boolean testAMBConnection() {

        long delayLastEvent = System.currentTimeMillis() - AMBReaderRc3.getLastEventMillis();



        if (delayLastEvent > 5000) {
            statusLine.setText(AMBReaderRc3.getDeviceAbbrev() + " disconnected");
            return false;
        } else {
            statusLine.setText(AMBReaderRc3.getDeviceAbbrev() + " last event " + delayLastEvent / 1000 + " secs ago");
            return true;
        }

    }
    Thread counterThread;
    DefaultCellEditor dce;
    Race todaysRace;
    Query eventQueryWithParams;
    Thread eventRefreshThread;
    String[] resultTableColumns = new String[]{msg("Drivername"),
        msg("Position"), msg("Rounds"),
        msg("StartingPosition"), msg("BestLapTime"), msg("FinishedTime"), msg("id")
    };
    JLapsConfig config;
    private List emptyResultList;
    private Image iconImage;

    public void updateResultsTable(String[][] resultList) {


        DefaultTableModel dtm = (DefaultTableModel) resultTable.getModel();
        if (resultList.length != dtm.getRowCount()) {
            dtm.setRowCount(resultList.length);
        }

        for (int x = 0; x < resultList.length; x++) {
            for (int y = 0; y < resultList[x].length; y++) {

                if (y == 5 && resultList[x][y] != null && !resultList[x][y].contains(":")) {
                    dtm.setValueAt(milisToTime(new Long(resultList[x][y])), x, y);
                } else {
                    dtm.setValueAt(resultList[x][y], x, y);
                }
            }
        }



    }

    public List getCurrentEventsList() {
        return eventList;
    }

    public void initResultsTable() {
        DefaultTableModel dtm = new ResultsTableDefaultTableModel();


        String[][] data = new String[1][7];
        dtm.setDataVector(data, resultTableColumns);
        resultTable.setModel(dtm);
        RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(dtm);
        resultTable.setRowSorter(sorter);

        ResultTableStringRenderer rtsr = new ResultTableStringRenderer();
        resultTable.setDefaultRenderer(String.class, rtsr);
        resultTable.setDefaultRenderer(Object.class, rtsr);
        resultTable.setDefaultRenderer(Integer.class, rtsr);

        if (resultTable.getColumnCount() > 6) {
            resultTable.removeColumn(resultTable.getColumnModel().getColumn(6));
        }
    }

    public void showFinishedResultInTable(int tableRow) {
        ResultTableStringRenderer rtsr = (ResultTableStringRenderer) resultTable.getCellRenderer(tableRow, 0);
        rtsr.setGrayRow(tableRow);
    }

    private void cleanGrayLines() {
        ResultTableStringRenderer rtsr = (ResultTableStringRenderer) resultTable.getCellRenderer(0, 0);
        rtsr.cleanGrayList();
    }

    private void setIcon() {

        iconImage = Toolkit.getDefaultToolkit().getImage("jlaps.jpg");
        this.getFrame().setIconImage(iconImage);

    }

    public JLapsView(SingleFrameApplication app) {
        super(app);
        this.app = app;

//        ApplicationContext ctxt = getContext();
//        System.out.println("Ctxt:" + ctxt.getLocalStorage().getDirectory());


        log.info(msg("StartingJLaps"));

        log.info("Connecting to database...");
        DBHelper.checkDBConnection();

        entityManager = java.beans.Beans.isDesignTime() ? null : javax.persistence.Persistence.createEntityManagerFactory(DBHelper.getPMName()).createEntityManager();

        DBHelper.checkInitialData(entityManager);

        config = new JLapsConfig();

        log.info("initilaizing components...");
        initComponents();

        initResultsTable();

        setIcon();

        raceProgressBar.setVisible(false);

        createEmptyResultsList();

        setRaceTitle(msg("racenotselected"));

        log.info("Reading configuration...");
        ambIPAddress.setText(config.getKey("AMB_IP"));
        ambPort.setText(config.getKey("AMB_PORT"));
        racePreparationText.setText(config.getKey("RACE_PREPARATION_TIME_SECS"));
        raceRuntimeText.setText(config.getKey("RACE_RUNTIME_TIME_MINS"));
        raceAnouncementTimeText.setText(config.getKey("RACE_ANONUNCEMENT_TIME_MINS"));

        log.info("Initializign sound system...");
        try {
            speaker = new Speaker(null, this, null);
            setSpeechVoicesCombo(speaker.listAllVoices());
            speaker = null;

            say("Welcome to Jlaps free RC timing software");
        } catch (Throwable t) {
            log.log(log.getLevel().SEVERE, "Unable to initialize sound system. Error: " + t);
        }
        
//        JSAPI.main(null);
        
        recordingsComboBox.removeAllItems();
        recordingsComboBox.addItem(msg("racestartshortlyvoice"));
        recordingsComboBox.addItem(msg("racestartvoice"));
        recordingsComboBox.addItem(msg("raceannouncement"));
        recordingsComboBox.addItem(msg("raceenvoice"));

        roundAllDrivers.setModel(new DefaultListModel());
        roundSelectedDrivers.setModel(new DefaultListModel());



        eventQueryWithParams = entityManager.createQuery("SELECT e FROM Event e WHERE RACE_ID = :RACE_ID and CATEGORY_ID = :CATEGORY_ID and GROUPS = :GROUPS and HEAT_NUMBER= :HEAT_NUMBER and FINAL_NUMBER=:FINAL_NUMBER order by ID desc LIMIT 0,1000").setMaxResults(300);
        smartTodaysRaceSelector();



        consoleScreen = new DeviceConsoleView(iconImage);
        log.info("Connecting to AMB...");
        ambReaderRef =
                new AMBReaderRc3(ambIPAddress.getText(), ambPort.getText(), entityManager, this, consoleScreen);
        ambContinousThread = new Thread(ambReaderRef);
        ambContinousThread.start();


        URL url = this.getClass().getResource("jlapsbanner.jpg");
        addsButton.setIcon(new ImageIcon(url));

        // tracking changes to save
        bindingGroup.addBindingListener(new AbstractBindingListener() {

            @Override
            public void targetChanged(Binding binding, PropertyStateEvent event) {
                // save action observes saveNeeded property
                setSaveNeeded(true);
            }
        });


        enableResultTableEditing(true);

        log.info("Setting latest font size...");
        String fontSize = config.getKey("FONT_SIZE");
        setFontSize(new Integer(fontSize).intValue(), mainPanel);
        raceSetupView = new RaceSetupView(app, this);
        setFontSize(new Integer(fontSize).intValue(), raceSetupView.getComponent());


        entityManager.getTransaction().begin();
        log.info("Started succesfully.");

    }

    public void setFontSize(int adjust, JComponent c) {

        Font fc = c.getFont();
        c.setFont(new Font(fc.getName(), fc.getStyle(), adjust));

        // sets tooltip font
        Font ftt = (Font) UIManager.get("ToolTip.font");
        ftt = new Font(ftt.getName(), ftt.getStyle(), adjust);
        UIManager.put("ToolTip.font", ftt);


        Component[] comps = c.getComponents();
        for (int i = 0; i < comps.length; i++) {
            if (comps[i] == null) {
                continue;
            }
            Font f = comps[i].getFont();
            comps[i].setFont(new Font(f.getName(), f.getStyle(), adjust));
            comps[i].repaint();

            if (comps[i] instanceof JPanel) {
                JPanel panel = (JPanel) comps[i];
                try {
                    TitledBorder b = (TitledBorder) panel.getBorder();
                    if (b != null) {
                        b.setTitleFont(new Font(f.getName(), f.getStyle(), adjust));
                    }
                } catch (Exception x) {
                }
            }
            if ((comps[i] instanceof JTabbedPane || comps[i] instanceof JPanel || comps[i] instanceof JScrollPane || comps[i] instanceof JViewport) && !c.equals(comps[i])) {
//                System.out.println("Invoiking " + comps[i].getName());

                setFontSize(adjust, (JComponent) comps[i]);
            } else if (comps[i] instanceof JTable) {
                JTable t = (JTable) comps[i];
                t.setRowHeight(adjust + 2);
                f = t.getTableHeader().getFont();
                t.getTableHeader().setFont(new Font(f.getName(), f.getStyle(), adjust));
//                System.out.println("Changing for table:" + t.getName());
            }

            c.repaint();
        }

    }

    public void setTodaysRaceFromOutside(Race r) {


        if (r == null) { //clear selection

            dynPanel.removeAll();
            dynPanel.repaint();
            currentSelection = null;
            todaysRace = null;
            setRaceTitle(msg("racenotselected"));
            return;
        } else {

            if (r.getCategories() == null || r.getGroups() == null) {
                return;
            }

            if (r.getCategories().trim().length() == 0 || r.getGroups().trim().length() == 0) {
                return;
            }

            log.info("Settign race " + r);
            currentSelection = new RaceSelection();

            List<Category> currentRaceCategories = RaceHelper.getCategoriesByIDs(r.getCategories());
            if (currentRaceCategories.size() == 0) {
                return;
            }

            Hashtable<Category, List> catGroups = RaceHelper.getRoundsList(currentRaceCategories, r.getGroups());

            setRaceTitle(r.getName());
            todaysRace = r;

            createRaceCategoriesButtons(catGroups, todaysRace);
        }
        refreshEvents();
        refreshResultTable();



    }

    private void startRefreshThread(long delay) {
        log.info("Startign refresh thread with delay of " + delay);
        eventRefreshThread = null;
        int duration = new Integer(raceRuntimeText.getText());
        int ann = new Integer(raceAnouncementTimeText.getText());

        boolean isFinalRound;
        if (currentSelection.getHeatsOrFinals()[0] == 0) {
            isFinalRound = true;
        } else {
            isFinalRound = false;
        }

        raceRunnerThread = new SimpleRaceRunner(this, rm, delay, duration, ann, todaysRace.getMinLapTime(), isFinalRound,
                todaysRace.getId(), currentSelection,
                ambReaderRef, resultsCalculator);
        raceRunnerThread.setRaceRunning(true);
        eventRefreshThread =
                new Thread(raceRunnerThread);
        eventRefreshThread.start();
    }

    @Action
    public Task refreshEvents() {
        return new RefreshEventsTask(getApplication());
    }

    private class RefreshEventsTask extends Task {

        RefreshEventsTask(org.jdesktop.application.Application app) {
            super(app);
        }

        @Override
        protected Object doInBackground() {

            if (todaysRace == null) {
                return null;
            }
            refreshEventsAndResultsAuto();
            return null;
        }

        @Override
        protected void finished() {
            setSaveNeeded(false);
        }
    }

    public void refreshEventsAndResultsAuto() {


        if (todaysRace == null) {
            return;
        }

        if (currentSelection.getCategory() == null) {
            return;
        }
        entityManager.clear();

        refreshEventsTable();

        refreshResultTable();

//        int groups = RaceHelper.getNumOfGroupsForRaceAndCat(todaysRace, getCurrentCategoryId());
        refreshAllEligibleDrivers();

        refreshSelectedDrivers();

    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = JLapsApp.getApplication().getMainFrame();
            aboutBox =
                    new JLapsAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }

        JLapsApp.getApplication().show(aboutBox);
    }

    private void startRace() {

        log.info("Starting current race");
        raceRunning = true;
        if (!testAMBConnection()) {
            JOptionPane.showMessageDialog(this.getComponent(),
                    msg("NoAMBSetup"), null, JOptionPane.ERROR_MESSAGE);
            stopRace(false);
            return;

        }

        cleanGrayLines();
        enableResultTableEditing(false);

        disableDuringRace(true);
        long sleepTime = new Integer(racePreparationText.getText()) * 1000;

        say(msg("racestartshortlyvoice"));
        statusLine.setText(msg("waitingforracesart"));


        if (entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().rollback();
        }

        resultsCalculator = new SimpleResultsCalculator(todaysRace.getId(),
                currentSelection, this, initialResultHash,
                todaysRace.getMinLapTime(), entityManager);


        startRefreshThread(sleepTime);

//        ambReaderRef.setRaceStart(todaysRace.getId(), getCurrentCategoryId(), getCurrentGroups(), getCurrentHeatOrFinal());

        startCounterButton.setText(msg("stopcounter"));
        say(msg("racestartvoice"));

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        mainPanel = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        startCounterButton = new javax.swing.JButton();
        refreshEventsButton = new javax.swing.JButton();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        resultTable = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        eventsTable = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        roundAllDrivers = new javax.swing.JList();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        roundSelectedDrivers = new javax.swing.JList();
        addSelectedDriver = new javax.swing.JButton();
        removeSelectedDriver = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        selectedDriverUp = new javax.swing.JButton();
        selectedDriverDown = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        dynPanel = new javax.swing.JPanel();
        addGroupsButton = new javax.swing.JButton();
        delGroupsButton = new javax.swing.JButton();
        raceProgressBar = new javax.swing.JProgressBar();
        jButton5 = new javax.swing.JButton();
        simulButton = new javax.swing.JToggleButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        ambIPAddress = new javax.swing.JTextField();
        ambPort = new javax.swing.JTextField();
        recordVoiceButton = new javax.swing.JButton();
        playVoiceButton = new javax.swing.JButton();
        recordingsComboBox = new javax.swing.JComboBox();
        jButton10 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        racePreparationText = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        raceRuntimeText = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        voicesCombo = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        raceAnouncementTimeText = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jButton12 = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        statusLine = new javax.swing.JLabel();
        ambStatusText = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        addsButton = new javax.swing.JButton();
        entityManager = java.beans.Beans.isDesignTime() ? null : javax.persistence.Persistence.createEntityManagerFactory(DBHelper.getPMName()).createEntityManager();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(net.jlaps.JLapsApp.class).getContext().getResourceMap(JLapsView.class);
        eventQuery = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery(resourceMap.getString("eventQuery.query")).setMaxResults(30); // NOI18N
        eventList = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(eventQuery.getResultList());
        raceQuery = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery(resourceMap.getString("raceQuery.query")); // NOI18N
        raceList = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(raceQuery.getResultList());
        categoryQuery = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery(resourceMap.getString("categoryQuery.query")); // NOI18N
        categoryList = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(categoryQuery.getResultList());
        jFrame1 = new javax.swing.JFrame();

        mainPanel.setBackground(resourceMap.getColor("mainPanel.background")); // NOI18N
        mainPanel.setAutoscrolls(true);
        mainPanel.setFont(resourceMap.getFont("mainPanel.font")); // NOI18N
        mainPanel.setName("mainPanel"); // NOI18N

        jTabbedPane2.setName("jTabbedPane2"); // NOI18N

        jPanel2.setName("jPanel2"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(net.jlaps.JLapsApp.class).getContext().getActionMap(JLapsView.class, this);
        startCounterButton.setAction(actionMap.get("startCounter")); // NOI18N
        startCounterButton.setText(resourceMap.getString("startCounterButton.text")); // NOI18N
        startCounterButton.setName("startCounterButton"); // NOI18N
        startCounterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startCounterButtonActionPerformed(evt);
            }
        });

        refreshEventsButton.setAction(actionMap.get("refreshEvents")); // NOI18N
        refreshEventsButton.setName("refreshEventsButton"); // NOI18N

        jTabbedPane3.setName("jTabbedPane3"); // NOI18N

        jPanel5.setName("jPanel5"); // NOI18N

        jScrollPane4.setName("jScrollPane4"); // NOI18N

        resultTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        resultTable.setEnabled(false);
        resultTable.setName("resultTable"); // NOI18N
        jScrollPane4.setViewportView(resultTable);

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        eventsTable.setName("eventsTable"); // NOI18N

        org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, eventList, eventsTable);
        org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${id}"));
        columnBinding.setColumnName("ID");
        columnBinding.setColumnClass(Long.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${identificationId}"));
        columnBinding.setColumnName("Identification Id");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${identificationType}"));
        columnBinding.setColumnName("Type");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${timestamp}l"));
        columnBinding.setColumnName("Time");
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane1.setViewportView(eventsTable);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                .addGap(26, 26, 26))
        );

        jTabbedPane3.addTab(resourceMap.getString("jPanel5.TabConstraints.tabTitle"), jPanel5); // NOI18N

        jPanel7.setName("jPanel7"); // NOI18N

        jScrollPane8.setName("jScrollPane8"); // NOI18N

        roundAllDrivers.setName("roundAllDrivers"); // NOI18N
        jScrollPane8.setViewportView(roundAllDrivers);

        jLabel18.setText(resourceMap.getString("jLabel18.text")); // NOI18N
        jLabel18.setName("jLabel18"); // NOI18N

        jScrollPane9.setName("jScrollPane9"); // NOI18N

        roundSelectedDrivers.setName("roundSelectedDrivers"); // NOI18N
        jScrollPane9.setViewportView(roundSelectedDrivers);

        addSelectedDriver.setAction(actionMap.get("addSelectedDriver")); // NOI18N
        addSelectedDriver.setName("addSelectedDriver"); // NOI18N

        removeSelectedDriver.setAction(actionMap.get("removeSelectedDriver")); // NOI18N
        removeSelectedDriver.setText(resourceMap.getString("removeSelectedDriver.text")); // NOI18N
        removeSelectedDriver.setName("removeSelectedDriver"); // NOI18N

        jLabel19.setText(resourceMap.getString("jLabel19.text")); // NOI18N
        jLabel19.setName("jLabel19"); // NOI18N

        selectedDriverUp.setAction(actionMap.get("selectedDriverUp")); // NOI18N
        selectedDriverUp.setText(resourceMap.getString("selectedDriverUp.text")); // NOI18N
        selectedDriverUp.setName("selectedDriverUp"); // NOI18N

        selectedDriverDown.setAction(actionMap.get("selectedDriverDown")); // NOI18N
        selectedDriverDown.setText(resourceMap.getString("selectedDriverDown.text")); // NOI18N
        selectedDriverDown.setName("selectedDriverDown"); // NOI18N

        jButton6.setAction(actionMap.get("printDriversIDsReport")); // NOI18N
        jButton6.setName("jButton6"); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(addSelectedDriver, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(removeSelectedDriver, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel18)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel19)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(selectedDriverUp, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(selectedDriverDown, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jButton6))
                .addGap(65, 65, 65))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(156, 156, 156)
                        .addComponent(addSelectedDriver)
                        .addGap(18, 18, 18)
                        .addComponent(removeSelectedDriver))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(jLabel19))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(85, 85, 85)
                                .addComponent(selectedDriverUp)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(selectedDriverDown)
                                .addGap(41, 41, 41)
                                .addComponent(jButton6)))))
                .addContainerGap())
        );

        jTabbedPane3.addTab(resourceMap.getString("jPanel7.TabConstraints.tabTitle"), jPanel7); // NOI18N

        jButton8.setAction(actionMap.get("cleanEventsAndResults")); // NOI18N
        jButton8.setName("jButton8"); // NOI18N

        jButton9.setAction(actionMap.get("saveEventAndResult")); // NOI18N
        jButton9.setName("jButton9"); // NOI18N

        jButton13.setAction(actionMap.get("printRoundReport")); // NOI18N
        jButton13.setName("jButton13"); // NOI18N

        jButton1.setAction(actionMap.get("openRaceSetup")); // NOI18N
        jButton1.setName("jButton1"); // NOI18N

        dynPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        dynPanel.setName("dynPanel"); // NOI18N
        dynPanel.setLayout(new java.awt.GridLayout(1, 0));

        addGroupsButton.setAction(actionMap.get("addGroup")); // NOI18N
        addGroupsButton.setName("addGroupsButton"); // NOI18N

        delGroupsButton.setAction(actionMap.get("delGroup")); // NOI18N
        delGroupsButton.setName("delGroupsButton"); // NOI18N

        raceProgressBar.setEnabled(false);
        raceProgressBar.setName("raceProgressBar"); // NOI18N

        jButton5.setAction(actionMap.get("printFinalReport")); // NOI18N
        jButton5.setName("jButton5"); // NOI18N

        simulButton.setAction(actionMap.get("managerSimulator")); // NOI18N
        simulButton.setName("simulButton"); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(dynPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(raceProgressBar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(startCounterButton)
                                        .addGap(100, 100, 100)
                                        .addComponent(jButton1))
                                    .addComponent(jTabbedPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(simulButton, 0, 0, Short.MAX_VALUE)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                            .addComponent(addGroupsButton)
                                            .addGap(18, 18, 18)
                                            .addComponent(delGroupsButton))
                                        .addComponent(jButton9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(refreshEventsButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton13, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGap(14, 14, 14))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dynPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(startCounterButton)
                            .addComponent(jButton1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTabbedPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addGroupsButton)
                            .addComponent(delGroupsButton))
                        .addGap(98, 98, 98)
                        .addComponent(jButton13)
                        .addGap(18, 18, 18)
                        .addComponent(jButton5)
                        .addGap(61, 61, 61)
                        .addComponent(refreshEventsButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                        .addComponent(simulButton)
                        .addGap(44, 44, 44)))
                .addComponent(raceProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9))
        );

        jTabbedPane2.addTab(resourceMap.getString("jPanel2.TabConstraints.tabTitle"), jPanel2); // NOI18N

        jPanel4.setName("jPanel4"); // NOI18N

        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        ambIPAddress.setName("ambIPAddress"); // NOI18N
        ambIPAddress.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                ambIPAddressFocusLost(evt);
            }
        });

        ambPort.setName("ambPort"); // NOI18N
        ambPort.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                ambPortFocusLost(evt);
            }
        });

        recordVoiceButton.setAction(actionMap.get("recordVoice")); // NOI18N
        recordVoiceButton.setText(resourceMap.getString("recordVoiceButton.text")); // NOI18N
        recordVoiceButton.setName("recordVoiceButton"); // NOI18N

        playVoiceButton.setAction(actionMap.get("playVoice")); // NOI18N
        playVoiceButton.setText(resourceMap.getString("playVoiceButton.text")); // NOI18N
        playVoiceButton.setName("playVoiceButton"); // NOI18N

        recordingsComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        recordingsComboBox.setName("recordingsComboBox"); // NOI18N

        jButton10.setAction(actionMap.get("testDeviceConnection")); // NOI18N
        jButton10.setName("jButton10"); // NOI18N

        jLabel7.setText(resourceMap.getString("jLabel7.text")); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N

        racePreparationText.setText(resourceMap.getString("racePreparationText.text")); // NOI18N
        racePreparationText.setName("racePreparationText"); // NOI18N
        racePreparationText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                racePreparationTextFocusLost(evt);
            }
        });

        jLabel8.setText(resourceMap.getString("jLabel8.text")); // NOI18N
        jLabel8.setName("jLabel8"); // NOI18N

        jLabel9.setText(resourceMap.getString("jLabel9.text")); // NOI18N
        jLabel9.setName("jLabel9"); // NOI18N

        raceRuntimeText.setText(resourceMap.getString("raceRuntimeText.text")); // NOI18N
        raceRuntimeText.setName("raceRuntimeText"); // NOI18N
        raceRuntimeText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                raceRuntimeTextFocusLost(evt);
            }
        });

        jLabel10.setText(resourceMap.getString("jLabel10.text")); // NOI18N
        jLabel10.setName("jLabel10"); // NOI18N

        voicesCombo.setName("voicesCombo"); // NOI18N

        jLabel11.setText(resourceMap.getString("jLabel11.text")); // NOI18N
        jLabel11.setName("jLabel11"); // NOI18N

        jLabel12.setText(resourceMap.getString("jLabel12.text")); // NOI18N
        jLabel12.setName("jLabel12"); // NOI18N

        jLabel13.setText(resourceMap.getString("jLabel13.text")); // NOI18N
        jLabel13.setName("jLabel13"); // NOI18N

        raceAnouncementTimeText.setText(resourceMap.getString("raceAnouncementTimeText.text")); // NOI18N
        raceAnouncementTimeText.setName("raceAnouncementTimeText"); // NOI18N
        raceAnouncementTimeText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                raceAnouncementTimeTextFocusLost(evt);
            }
        });

        jLabel14.setText(resourceMap.getString("jLabel14.text")); // NOI18N
        jLabel14.setName("jLabel14"); // NOI18N

        jButton12.setAction(actionMap.get("testVoice")); // NOI18N
        jButton12.setName("jButton12"); // NOI18N

        jLabel15.setText(resourceMap.getString("jLabel15.text")); // NOI18N
        jLabel15.setName("jLabel15"); // NOI18N

        jButton3.setAction(actionMap.get("increaseFont")); // NOI18N
        jButton3.setName("jButton3"); // NOI18N

        jButton4.setAction(actionMap.get("decreaseFont")); // NOI18N
        jButton4.setName("jButton4"); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel1))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(voicesCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addComponent(jButton12))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(ambIPAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(44, 44, 44)
                                .addComponent(jLabel2)
                                .addGap(2, 2, 2)
                                .addComponent(ambPort, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton10))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(recordingsComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(recordVoiceButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(playVoiceButton))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(raceRuntimeText, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                            .addComponent(racePreparationText, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel10)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(raceAnouncementTimeText, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addComponent(jLabel14))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton3)
                        .addGap(18, 18, 18)
                        .addComponent(jButton4)))
                .addContainerGap(104, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(ambIPAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jButton10)
                    .addComponent(ambPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(recordingsComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(recordVoiceButton)
                    .addComponent(playVoiceButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jButton12)
                    .addComponent(voicesCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(racePreparationText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(raceRuntimeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14)
                    .addComponent(raceAnouncementTimeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jButton3)
                    .addComponent(jButton4))
                .addContainerGap(253, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab(resourceMap.getString("jPanel4.TabConstraints.tabTitle"), jPanel4); // NOI18N

        statusLine.setText(resourceMap.getString("statusLine.text")); // NOI18N
        statusLine.setName("statusLine"); // NOI18N

        ambStatusText.setText(resourceMap.getString("ambStatusText.text")); // NOI18N
        ambStatusText.setName("ambStatusText"); // NOI18N

        jButton2.setAction(actionMap.get("openDeviceConsole")); // NOI18N
        jButton2.setName("jButton2"); // NOI18N

        addsButton.setBackground(resourceMap.getColor("addsButton.background")); // NOI18N
        addsButton.setFont(resourceMap.getFont("addsButton.font")); // NOI18N
        addsButton.setForeground(resourceMap.getColor("addsButton.foreground")); // NOI18N
        addsButton.setIcon(resourceMap.getIcon("addsButton.icon")); // NOI18N
        addsButton.setText(resourceMap.getString("addsButton.text")); // NOI18N
        addsButton.setBorder(null);
        addsButton.setBorderPainted(false);
        addsButton.setFocusPainted(false);
        addsButton.setName("addsButton"); // NOI18N
        addsButton.setOpaque(false);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(addsButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 629, Short.MAX_VALUE)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(statusLine, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 161, Short.MAX_VALUE)
                        .addComponent(ambStatusText, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2))
                    .addComponent(jTabbedPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 629, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 543, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2)
                    .addComponent(statusLine)
                    .addComponent(ambStatusText, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.setName("jTabbedPane1"); // NOI18N

        jFrame1.setName("jFrame1"); // NOI18N

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setComponent(mainPanel);

        bindingGroup.bind();
    }// </editor-fold>//GEN-END:initComponents

    @Action
    public void startCounter() {

        log.info("Start race button pressed");
        isLapPostTime = false;

        if (todaysRace == null) {
            JOptionPane.showMessageDialog(this.getComponent(),
                    msg("norace"), null, JOptionPane.ERROR_MESSAGE);

            return;

        }

        if (ambIPAddress.getText() == null && ambPort == null) {
            JOptionPane.showMessageDialog(this.getComponent(),
                    msg("NoAMBSetup"), null, JOptionPane.ERROR_MESSAGE);

            return;
        }

        if (raceRunnerThread == null) {
            startRace();

        } else {
            /// race end
            stopRace(true);
        }

    }

    public void stopRace(boolean normalStop) {

        log.info("Stopping race");

        if (normalStop) {
            say(msg("raceenvoice"));
        }

        if (resultsCalculator != null) {
            resultsCalculator.finishAllDrivers();
        }
        ambReaderRef.stopRace();
        if (raceRunnerThread != null) {
            raceRunnerThread.setRaceRunning(false);
        }



        raceRunnerThread = null;
        if (normalStop && resultsCalculator != null) {
            resultsCalculator.saveResults(entityManager);
        }

        refreshEventsAndResultsAuto();

        resultsCalculator = null;
        startCounterButton.setText(msg("startcounter"));


        eventRefreshThread =
                null;
        enableResultTableEditing(true);

        raceRunning =
                false;

        disableDuringRace(false);
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }

    }

    private void startCounterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startCounterButtonActionPerformed
}//GEN-LAST:event_startCounterButtonActionPerformed

    private void ambIPAddressFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ambIPAddressFocusLost
        config.saveKey("AMB_IP", ambIPAddress.getText());
    }//GEN-LAST:event_ambIPAddressFocusLost

    private void ambPortFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ambPortFocusLost
        config.saveKey("AMB_PORT", ambPort.getText());
    }//GEN-LAST:event_ambPortFocusLost

    private void racePreparationTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_racePreparationTextFocusLost
        config.saveKey("RACE_PREPARATION_TIME_SECS", racePreparationText.getText());

    }//GEN-LAST:event_racePreparationTextFocusLost

    private void raceRuntimeTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_raceRuntimeTextFocusLost
        config.saveKey("RACE_RUNTIME_TIME_MINS", raceRuntimeText.getText());
    }//GEN-LAST:event_raceRuntimeTextFocusLost

    private void raceAnouncementTimeTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_raceAnouncementTimeTextFocusLost
        config.saveKey("RACE_ANONUNCEMENT_TIME_MINS", raceAnouncementTimeText.getText());
    }//GEN-LAST:event_raceAnouncementTimeTextFocusLost

    @Action
    public void cleanEventsAndResults() {

        log.info("Cleaning events and results");
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }

        Query q = entityManager.createNativeQuery("DELETE FROM DEVICE_EVENT e where e.race_id = :race_id and e.category_id = :category_id and e.final_number = :final_number and e.heat_number = :heat_number");
        q.setParameter("race_id", todaysRace.getId()).setParameter("category_id", currentSelection.getCategory().getId()).setParameter("heat_number", currentSelection.getHeatsOrFinals()[0]).setParameter("final_number", currentSelection.getHeatsOrFinals()[1]).executeUpdate();
        q = entityManager.createNativeQuery("DELETE FROM RACE_RESULT r where r.race_id = :race_id " +
                "and r.category_id = :category_id and r.final_number = :final_number and r.heat_number = :heat_number");
        q.setParameter("race_id", todaysRace.getId()).setParameter("category_id", currentSelection.getCategory().getId()).setParameter("heat_number", currentSelection.getHeatsOrFinals()[0]).setParameter("final_number", currentSelection.getHeatsOrFinals()[1]).executeUpdate();

        Iterator it = eventList.iterator();
        while (it.hasNext()) {
            eventList.remove(0);
        }

        updateResultsTable(new String[0][3]);

        setSaveNeeded(true);
    }

    @Action
    public void recordVoice() {

        log.info("Recording....");
        if (audioManagerRecorder == null) {
            audioManagerRecorder = new AudioManagerRecorder();
        }
        System.out.println("Text:" + recordVoiceButton.getText());

        if (recordVoiceButton.getText().equalsIgnoreCase(msg("recordVoiceButton.text"))) {
            audioManagerRecorder.prepareRecording((String) recordingsComboBox.getSelectedItem());
            audioManagerRecorder.startRecording();
            recordVoiceButton.setText(msg("stop"));

        } else if (recordVoiceButton.getText().equalsIgnoreCase(msg("stop"))) {
            audioManagerRecorder.stopRecording();
            recordVoiceButton.setText(msg("recordVoiceButton.text"));
            audioManagerRecorder =
                    null;
        }

    }

    @Action
    public void playVoice() {

        log.info("Playing voice");
        audioManagerPlayer = new AudioManagerPlayer(recordingsComboBox.getSelectedItem().toString());
        ((Thread) audioManagerPlayer).start();
    }

    @Action
    public void testVoice() {
        log.info("testing voice");
        say("Hello user, I am JLaps voice syntetizer");
    }

    @Action
    public void testDeviceConnection() {
        testAMBConnection();
    }

    @Action
    public void showResultWindow() {
        log.warning("Not implemented yet");
    }
    private boolean saveNeeded = false;

    public boolean isSaveNeeded() {
        return saveNeeded;
    }

    public void setSaveNeeded(boolean b) {
        boolean old = isSaveNeeded();
        this.saveNeeded = b;
        firePropertyChange("saveNeeded", old, isSaveNeeded());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addGroupsButton;
    private javax.swing.JButton addSelectedDriver;
    private javax.swing.JButton addsButton;
    private javax.swing.JTextField ambIPAddress;
    private javax.swing.JTextField ambPort;
    private javax.swing.JLabel ambStatusText;
    private java.util.List<net.jlaps.entity.Category> categoryList;
    private javax.persistence.Query categoryQuery;
    private javax.swing.JButton delGroupsButton;
    private javax.swing.JPanel dynPanel;
    private javax.persistence.EntityManager entityManager;
    private java.util.List<net.jlaps.entity.Event> eventList;
    private javax.persistence.Query eventQuery;
    private javax.swing.JTable eventsTable;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton playVoiceButton;
    private javax.swing.JTextField raceAnouncementTimeText;
    private java.util.List<net.jlaps.entity.Race> raceList;
    private javax.swing.JTextField racePreparationText;
    private javax.swing.JProgressBar raceProgressBar;
    private javax.persistence.Query raceQuery;
    private javax.swing.JTextField raceRuntimeText;
    private javax.swing.JButton recordVoiceButton;
    private javax.swing.JComboBox recordingsComboBox;
    private javax.swing.JButton refreshEventsButton;
    private javax.swing.JButton removeSelectedDriver;
    private javax.swing.JTable resultTable;
    private javax.swing.JList roundAllDrivers;
    private javax.swing.JList roundSelectedDrivers;
    private javax.swing.JButton selectedDriverDown;
    private javax.swing.JButton selectedDriverUp;
    private javax.swing.JToggleButton simulButton;
    private javax.swing.JButton startCounterButton;
    private javax.swing.JLabel statusLine;
    private javax.swing.JComboBox voicesCombo;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
//    private final Timer messageTimer;
//    private final Timer busyIconTimer;
//    private final Icon idleIcon;
//    private final Icon[] busyIcons = new Icon[15];
//    private int busyIconIndex = 0;
    private JDialog aboutBox;

    public void setAmbStatus(String text) {
        ambStatusText.setText(text);
        ambStatusText.setHorizontalAlignment(SwingConstants.RIGHT);
    }

    @Action
    public void printRoundReport() {
        log.info("Prepating round report");
        if (reportViewer == null) {
            reportViewer = new JasperReportViewer();
        }

        HashMap params = new HashMap();
        params.put("subDataSourceResults", new JRBeanCollectionDataSource(resultsTableList));

        params.put("subDataSourceDetails", new JRBeanCollectionDataSource(
                RaceHelper.createDetailResultsList(eventList, todaysRace.getMinLapTime())));
        params.put("subDataSourceProgress", new JRBeanCollectionDataSource(
                ResultsHelper.calculateProgressResults(todaysRace, currentSelection, entityManager)));
        params.put("categoryName", currentSelection.getCategory().getName());
        params.put("heat", currentSelection.getHeatsOrFinals()[0]);
        params.put("final", currentSelection.getHeatsOrFinals()[1]);
        params.put("groups", currentSelection.getGroups());
        ArrayList raceLists = new ArrayList();
        raceLists.add(todaysRace);
        log.info("Invoking report generator");
        reportViewer.printReport("rounds", params, new JRBeanCollectionDataSource(raceLists));


    }

    @Action
    public void addSelectedDriver() {

        log.info("Adding selected drivers to group");
        int[] selected = roundAllDrivers.getSelectedIndices();
        DefaultListModel dlm = (DefaultListModel) roundSelectedDrivers.getModel();
        DefaultListModel dlm2 = (DefaultListModel) roundAllDrivers.getModel();
        List<DriverModel> toStore = new ArrayList<DriverModel>();
        for (int i = 0; i <
                selected.length; i++) {
            dlm.addElement(dlm2.getElementAt(selected[i]));
            toStore.add((DriverModel) dlm2.getElementAt(selected[i]));
            dlm2.removeElementAt(selected[i]);
        }

        DBHelper.storeDriverModelList(todaysRace.getId(), currentSelection,
                (DefaultListModel) roundSelectedDrivers.getModel(), entityManager);
        setSaveNeeded(true);
    }

    @Action
    public void removeSelectedDriver() {
        log.info("Removing selected drivers from group");
        int[] selected = roundSelectedDrivers.getSelectedIndices();
        DefaultListModel dlm = (DefaultListModel) roundSelectedDrivers.getModel();
        DefaultListModel dlm2 = (DefaultListModel) roundAllDrivers.getModel();
        for (int i = 0; i <
                selected.length; i++) {
            dlm2.addElement((DriverModel) dlm.getElementAt(selected[i]));
            dlm.removeElementAt(selected[i]);

        }

        DBHelper.storeDriverModelList(todaysRace.getId(), currentSelection, dlm, entityManager);

        setSaveNeeded(true);

    }

    @Action
    public void generateAutoPositions() {
        log.warning("not implemented yeat");
    }

    @Action
    public void openRaceSetup() {
        log.info("Openning race setup window");
        if (raceSetupView == null) {
            raceSetupView = new RaceSetupView(app, this);
            setFontSize(mainPanel.getFont().getSize(), raceSetupView.getComponent());
        }

        raceSetupView.getApplication().show(raceSetupView);
    }

    @Action
    public void openDeviceConsole() {
        log.info("Openning device console");
        if (consoleScreen == null) {
            consoleScreen = new DeviceConsoleView(iconImage);
        }

        consoleScreen.setVisible(true);

    }

    private class CategoryAL implements ActionListener {

        public CategoryAL() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            log.info("Category AL invoked");
            currentSelection.setCategory(
                    DBHelper.getCategoryById(new Long(e.getActionCommand()), entityManager));
            recreateGroupsButton(todaysRace);
            refreshResultTable();
            refreshEventsTable();
            refreshSelectedDrivers();
            refreshAllEligibleDrivers();
        }
    }

    private void refreshSelectedDrivers() {

        log.info("Refreshes selected drivers");
        DefaultListModel dlm = (DefaultListModel) roundSelectedDrivers.getModel();
        dlm.clear();
        List<DriverModel> selectedDrivers = DBHelper.getSelectedRacersForRaceGroupCat(todaysRace.getId(),
                currentSelection, entityManager);
        Iterator<DriverModel> it = selectedDrivers.iterator();
        while (it.hasNext()) {
            dlm.addElement(it.next());
        }
        if (resultTable.getRowCount() > 0) {
            roundAllDrivers.setEnabled(false);
            roundSelectedDrivers.setEnabled(false);
        }


    }

    private void refreshAllEligibleDrivers() {

        log.info("Refreshing all eligible drivers");
        roundAllDrivers.setEnabled(true);
        roundSelectedDrivers.setEnabled(true);
        DefaultListModel dlm = DBHelper.getAllMatchingDriversForRound(todaysRace.getId(),
                currentSelection, entityManager);

        roundAllDrivers.setModel(dlm);

        if (resultTable.getRowCount() > 0) {
            roundAllDrivers.setEnabled(false);
            roundSelectedDrivers.setEnabled(false);
        }

    }

    private class FinalsAL implements ActionListener {

        public FinalsAL() {
        }

        public void actionPerformed(ActionEvent e) {
            log.info("Finals AL invoked");
            int id = new Integer(e.getActionCommand());
            currentSelection.setHeatsOrFinals(new int[]{0, id});
            recreateGroupsButton(todaysRace);
            refreshResultTable();
            refreshEventsTable();
            refreshAllEligibleDrivers();
            refreshSelectedDrivers();

        }
    }

    private class GroupsAL implements ActionListener {

        public GroupsAL() {
        }

        public void actionPerformed(ActionEvent e) {
            log.info("Groups AL invoked");
            currentSelection.setGroups(e.getActionCommand());
            refreshResultTable();
            refreshEventsTable();
            refreshAllEligibleDrivers();
            refreshSelectedDrivers();

        }
    }

    private class HeatsAL implements ActionListener {

        public HeatsAL() {
        }

        public void actionPerformed(ActionEvent e) {
            log.info("Heats AL invoked");
            int id = new Integer(e.getActionCommand());
            currentSelection.setHeatsOrFinals(new int[]{id, 0});
            recreateGroupsButton(todaysRace);
            refreshResultTable();
            refreshEventsTable();
            refreshAllEligibleDrivers();
            refreshSelectedDrivers();
        }
    }

    @Action
    public void addGroup() {
        log.info("Adding group");
        int count = RaceHelper.getNumOfGroupsForRaceAndCat(todaysRace, currentSelection.getCategory().getId());
        System.out.println("Number of groups:" + count);
        count++;

        delGroupsButton.setEnabled(true);
        currentSelection.setGroups(
                RaceHelper.updateGroupsNumberForRaceCats(todaysRace, currentSelection.getCategory().getId(), count));
        log.info("New groups:" + currentSelection.getGroups());
        recreateGroupsButton(todaysRace);
    }

    @Action
    public void delGroup() {
        log.info("Deleting group");
        int count = RaceHelper.getNumOfGroupsForRaceAndCat(todaysRace, currentSelection.getCategory().getId());
        if (count <= 1) {
            delGroupsButton.setEnabled(false);
            return;

        }

        count--;
        currentSelection.setGroups(
                RaceHelper.updateGroupsNumberForRaceCats(todaysRace, currentSelection.getCategory().getId(), count));
        log.info("New groups:" + currentSelection.getGroups());
        recreateGroupsButton(todaysRace);
    }

    public void runRaceProgressBar(int durationSecs) {
        if (pbr != null) {
            stopRaceProgressBar();
        }

        pbr = new ProgressBarRunner(durationSecs, raceProgressBar);
        pbr.start();
    }

    public void stopRaceProgressBar() {
        if (pbr != null) {
            pbr.stopNow();
        }

        pbr = null;
    }

    @Action
    public void selectedDriverUp() {
    }

    @Action
    public void selectedDriverDown() {
    }

    @Action
    public void increaseFont() {

        log.info("Increasing font");
        setFontSize(mainPanel.getFont().getSize() + 1, mainPanel);
        setFontSize(mainPanel.getFont().getSize() + 1, raceSetupView.getComponent());
        config.saveKey("FONT_SIZE", new Integer(mainPanel.getFont().getSize() + 1).toString());
        System.out.println("curr size: " + mainPanel.getFont().getSize());
    }

    @Action
    public void decreaseFont() {
        log.info("Decreasing font");
        setFontSize(mainPanel.getFont().getSize() - 1, mainPanel);
        setFontSize(mainPanel.getFont().getSize() - 1, raceSetupView.getComponent());
        System.out.println("curr size: " + mainPanel.getFont().getSize());
        config.saveKey("FONT_SIZE", new Integer(mainPanel.getFont().getSize() - 1).toString());
    }

    @Action
    public void printFinalReport() {

        log.info("Printing final report");
        if (reportViewer == null) {
            reportViewer = new JasperReportViewer();
        }

        HashMap params = new HashMap();
        params.put("subDataSourceFinal", new JRBeanCollectionDataSource(
                ResultsHelper.calculateProgressResults(todaysRace, currentSelection, entityManager)));
        params.put("categoryName", currentSelection.getCategory().getName());
        ArrayList raceLists = new ArrayList();
        raceLists.add(todaysRace);
        log.info("Invoking report generator");
        reportViewer.printReport("final_results", params, new JRBeanCollectionDataSource(raceLists));

    }

    @Action
    public void managerSimulator() {
        if (simulator == null) {

            simulator = new SimulatorThread();
            simulator.start();
            simulButton.setText(msg("StopAMBsimulator"));

        } else {
            simulator.canRun = false;
            simulator = null;
            simulButton.setText(msg("StartAMBsimulator"));
        }
    }

    @Action
    public void printDriversIDsReport() {
        

        log.info("Printing initial report");
        if (reportViewer == null) {
            reportViewer = new JasperReportViewer();
        }

        ArrayList driversList = RaceHelper.createInitialListofDrivers(todaysRace,entityManager);
        log.info("Invoking report generator");
        reportViewer.printReport("drivers_and_ids", null, new JRBeanCollectionDataSource(driversList));

    }
}
