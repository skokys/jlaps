/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.simulator;

/**
 *
 * @author lskokan
 */
public class SimulatorThread extends Thread {

    public boolean canRun;
    @Override
    public void run() {
        canRun = true;
        new AMBSimulator(this);
        
    }

    @Override
    public synchronized void start() {
        super.start();
    }
    
    

}
