/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.calculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import net.jlaps.entity.Category;
import net.jlaps.entity.Race;
import net.jlaps.entity.RaceResult;
import net.jlaps.entity.helpers.ResultsComparator;
import net.jlaps.utils.tableutils.RaceSelection;

/**
 *
 * @author lskokan
 */
public class ResultsHelper {

    public static List calculateProgressResults(Race todaysRace, RaceSelection currentSelection,
            EntityManager e) {

        Query q;
        if (currentSelection.getHeatsOrFinals()[0] != 0) {
            q = e.createNamedQuery("RaceResults.progress.heat");
        } else {
            q = e.createNamedQuery("RaceResults.progress.final");
        }
        List<RaceResult> resultsList = q.setParameter("race_id", todaysRace.getId()).setParameter("category_id", currentSelection.getCategory().getId()).getResultList();

        boolean heatsOrFinals = false;
        if (currentSelection.getHeatsOrFinals()[0] != 0) {
            heatsOrFinals = true;
        } else {
            heatsOrFinals = false;
        }
        resultsList = mergeResultsFromMoreRounds(resultsList, currentSelection.getCategory(), heatsOrFinals);

        return resultsList;

    }

    private static RaceResult getBestResultForDriverMerged(List<RaceResult> resultsForOneDriver, Category cat, boolean heatsOrFinals) {
        RaceResult bestResult = new RaceResult();

        Collections.sort(resultsForOneDriver, new ResultsComparator(true));
        int numberOfRoundsToCalculate = 0;
        if (heatsOrFinals) {
            numberOfRoundsToCalculate = cat.getBestHeats();
        } else {
            numberOfRoundsToCalculate = cat.getBestFinals();
        }
        for (int i = 0; i < numberOfRoundsToCalculate; i++) {

            if (i + 1 > resultsForOneDriver.size()) {
                break;
            }
            RaceResult r = resultsForOneDriver.get(i);

            if (bestResult.getPosition() != null) {
                bestResult.setPosition(bestResult.getPosition() + r.getPosition());
            } else {
                bestResult.setPosition(r.getPosition());
            }
            if (bestResult.getDriverName() == null) {
                bestResult.setDriverName(r.getDriverName());
            }
            if (bestResult.getCompleteRoundsAchieved() != null) {
                bestResult.setCompleteRoundsAchieved(bestResult.getCompleteRoundsAchieved() + r.getCompleteRoundsAchieved());
            } else {
                bestResult.setCompleteRoundsAchieved(r.getCompleteRoundsAchieved());
            }
            if (bestResult.getBestRoundTime() != null) {
                if (r.getBestRoundTime() < bestResult.getBestRoundTime()) {
                    bestResult.setBestRoundTime(r.getBestRoundTime());
                }
            } else {
                bestResult.setBestRoundTime(r.getBestRoundTime());
            }
        }
        return bestResult;
    }

    private static ArrayList<String> getListOfDrivers(List input) {


        ArrayList<String> drivers = new ArrayList<String>();

        Iterator<RaceResult> i = input.iterator();
        while (i.hasNext()) {
            RaceResult r = i.next();
            String driver = r.getDriverName();
            if (!drivers.contains(driver)) {
                drivers.add(driver);
            }
        }
        return drivers;
    }

    private static Vector getResultsForDriver(String next, List input) {
        Vector<RaceResult> resultsForOneDriver = new Vector<RaceResult>();

        Iterator<RaceResult> i = input.iterator();
        while (i.hasNext()) {
            RaceResult r = i.next();
            if (next.equalsIgnoreCase(r.getDriverName())) {
                resultsForOneDriver.add(r);
            }
        }
        return resultsForOneDriver;
    }

    private static List mergeResultsFromMoreRounds(List input, Category cat, boolean heatsOrFinals) {

        List<RaceResult> results = new ArrayList<RaceResult>();
        List<String> driversIds = getListOfDrivers(input);

        Iterator<String> e = driversIds.iterator();
        while (e.hasNext()) {
            Vector resultsForOneDriver = getResultsForDriver(e.next(), input);
            RaceResult bestResult = getBestResultForDriverMerged(resultsForOneDriver, cat, heatsOrFinals);
            results.add(bestResult);
        }

        Collections.sort(results, new ResultsComparator(true));
        recalculatePositions(results);
        return results;
    }

    private static void recalculatePositions(List<RaceResult> results) {
        Iterator<RaceResult> it = results.iterator();
        int i = 1;
        while (it.hasNext()) {
            RaceResult r = it.next();
            r.setPosition(i);
            i++;
        }
    }
}
