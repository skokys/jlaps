/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.calculator;

import javax.persistence.EntityManager;

/**
 *
 * @author skoky
 */
interface RaceCalculator {

    public void setEventToProcess(String identification, Long timestamp);
    public void saveResults(EntityManager entityManager);
    public String getName();
    public String getDescription();
    
}
