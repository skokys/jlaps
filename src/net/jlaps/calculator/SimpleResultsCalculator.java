/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.calculator;

import net.jlaps.utils.*;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import javax.persistence.EntityManager;
import net.jlaps.JLapsView;
import net.jlaps.utils.tableutils.RaceSelection;
import net.jlaps.entity.RaceResult;
import net.jlaps.entity.helpers.ResultsComparator;
import net.jlaps.readers.AMBReaderRc3;

/**
 *
 * @author skoky
 */
public class SimpleResultsCalculator extends Thread implements Runnable, RaceCalculator {
 
    private String ident;
    private Long timestamp;
    private Long raceId;
    private Long categoryId;
    private String groups;    
    private int[] heatOrRound;
    private int lastPosition;
    
    private static Hashtable<String,String> idModelsDriversHash = new Hashtable();
    private static EntityManager em;
    
    Hashtable<String,RaceResult> resultsHash;
    Hashtable<String,Long> lastTimeHash = new Hashtable<String, Long>();
    
    private JLapsView view;

    private int MAX_VALUE=9999;
    private int MAX_POSITION=10;
    private Integer minLapTime;
    private Long raceStartTime;
    
    
    
    public SimpleResultsCalculator( Long raceId, RaceSelection race,JLapsView v, 
            Hashtable<String,RaceResult> initialResultsHash, Integer minLapTime, EntityManager e) {
        super();
        if ( initialResultsHash == null )
            resultsHash = new Hashtable<String,RaceResult>();
        else
            this.resultsHash = initialResultsHash;
        this.raceId = raceId;
        this.categoryId = race.getCategory().getId();
        this.groups = race.getGroups();
        this.heatOrRound = race.getHeatsOrFinals();
        lastPosition=1;
        this.view = v;
        if ( minLapTime==null)
            this.minLapTime=0;
        else
            this.minLapTime = minLapTime;
        this.em = e;
    }

    public void finishAllDrivers() {

        Long finishedTime = System.currentTimeMillis();
        Enumeration<String> e = resultsHash.keys();
        while (e.hasMoreElements()) {
            String id = e.nextElement();
            RaceResult res = resultsHash.get(id);
            
            if ( res == null ) continue;
            if ( res.getFinishedTime() == null ) {
                
                if ( isThisRoundFinals()) {
                    res.setFinishTime( finishedTime - raceStartTime);
                } else
                    res.setFinishTime(finishedTime - res.getStartTime());
                resultsHash.put(id, res);
            }
        }

//        updatePositions();
        view.updateResultsTable(convertResultsHashToTableArray(resultsHash,view,true));
        // view.refreshEventsTable();
        
    }

    @Override
    public void run() {
        super.run();
        synchronized (this) {
            addNewEventToResults();
        }
        
        
        
    }

    @Override
    public void setEventToProcess(String identification, Long timestamp) {
        this.ident = identification;
        this.timestamp = timestamp;
    }

    void setRaceStartTime(long currentTimeMillis) {
        this.raceStartTime = currentTimeMillis;
    }

    private void addNewEventToResults() {
        RaceResult r;
        
        // find out results in results hash
        if ( resultsHash.containsKey(ident)) {
            r = resultsHash.get(ident);
            
            if ( r.getIsFinished() != null)
                return;

                // is its heat, and time of the heat is over, remark the result as finsihed

                // remambers last timestamp of last event per racer; If already done, compares to the 
                // best lap time. If better, updates
                Long lastTime;
                float f;

                if ( lastTimeHash.containsKey(ident) && r.getBestRoundTime() != null) {// if its not first lap
                    lastTime = lastTimeHash.get(ident);
                    if ( (timestamp-lastTime)/1000 < minLapTime  ) { // invalid data - ignore
                        return;                    
                    }
                    Float currentBestTime = r.getBestRoundTime();
                    if ( currentBestTime*1000 > timestamp-lastTime) { // update the best time
                        f = timestamp-lastTime;

                        r.setBestRoundTime(f/1000);   
                    } 
                } else if (lastTimeHash.containsKey(ident) && r.getBestRoundTime() == null ) {
                    lastTime = lastTimeHash.get(ident);
                    f = timestamp-lastTime;
                    if ( f/1000 < minLapTime) // invalid data - ignore
                        return;
                    r.setBestRoundTime(f/1000);
                }

                lastTimeHash.put(ident, timestamp);


                // update rounds
                int rounds = r.getCompleteRoundsAchieved();
                rounds++;
                r.setCompleteRoundsAchieved(rounds);
                if ( view.isLapPostTime()) {
                     System.out.println("Setting finihed");
                     
                     if ( isThisRoundFinals()) 
                        r.setFinishTime(timestamp - raceStartTime );
                     else
                        r.setFinishTime(timestamp - r.getStartTime()); 
                     r.setIsFinished(true);
                }
                resultsHash.put(ident, r);            
            
            
        } else { // new races in the race results
            r = new RaceResult(raceId,categoryId,groups,heatOrRound);
            r.setDriverName(convertIdentificationToNameAndModel(ident,categoryId));
            r.setStartingPosition(lastPosition);
            r.setCompleteRoundsAchieved(0);
            
            if ( isThisRoundFinals())
                r.setStartTime(raceStartTime);
            else
                r.setStartTime(timestamp);
            
            lastPosition++;
            resultsHash.put(ident, r);
        }
            
         // updates positions
//        updatePositions();
        
        view.updateResultsTable(convertResultsHashToTableArray(resultsHash,view,false));
        view.refreshEventsTable();
        
    }
    
    
    public static String convertIdentificationToNameAndModel(String identificationId, Long currentCategoryId) {
        
        if ( !idModelsDriversHash.containsKey(identificationId)) {
            
            String driverModelName = DBHelper.getDriverModelName(identificationId, currentCategoryId, em);
            if ( driverModelName != null ) {
                idModelsDriversHash.put(identificationId, driverModelName);
            } else
                idModelsDriversHash.put(identificationId, 
                        AMBReaderRc3.getDeviceAbbrev() + " ID:" + identificationId);
        } 
        
        return idModelsDriversHash.get(identificationId);
        
                
    }

    public static String[][] convertResultsHashToTableArray(Hashtable<String,RaceResult> rh, JLapsView view, boolean afterFinish) {

        // creates array for results table
        String[][] resultsArray = new String[rh.size()][6];
        Enumeration<RaceResult> y = rh.elements();
        Vector<RaceResult> sortedResults = new Vector<RaceResult>();
        while(y.hasMoreElements())
            sortedResults.add(y.nextElement());
        Collections.sort(sortedResults, new ResultsComparator(afterFinish));
        
        Enumeration<RaceResult> x = sortedResults.elements();
        
        RaceResult r;
        for(int i=0;i<rh.size();i++) {
            r = x.nextElement();
            if ( r.getDriverName() != null)
                resultsArray[i][0] = r.getDriverName();
//            if ( r.getPosition() != null )
//                resultsArray[i][1] = r.getPosition().toString();
                resultsArray[i][1] = new Integer(i+1).toString();
                r.setPosition(i+1);
                
            if ( r.getCompleteRoundsAchieved() != null )
                resultsArray[i][2] = r.getCompleteRoundsAchieved().toString();
            if ( r.getStartingPosition() != null )
                resultsArray[i][3] = r.getStartingPosition().toString();
            if ( r.getBestRoundTime() != null ) {
                resultsArray[i][4] = r.getBestRoundTime().toString();
            }
            if ( r.getFinishedTime() != null ) {
                resultsArray[i][5] = r.getFinishedTime().toString();
            }
            
            if ( r.getIsFinished()!=null) {
                view.showFinishedResultInTable(i);
            } 
            
        }
        
        return resultsArray;
                
    }


    private Vector<String> getBestResult() {
            
            
            int currentCompleteRounds,bestCompletedRoundsResult=0;
            float currentLapTime,bestLapTime = MAX_VALUE;
            
            String id;
            RaceResult r;
            
            Vector<String> results = new Vector<String>();
            
            Enumeration<String> resultIds = resultsHash.keys();
            //iterates to results to search for a best result
            for(int i = 0; i<resultsHash.size();i++) {
                id = resultIds.nextElement();
                r = resultsHash.get(id);
                if ( r == null )
                    continue;
                if ( r.getPosition()!=MAX_POSITION)
                    continue;
                currentCompleteRounds = r.getCompleteRoundsAchieved().intValue();
                if ( r.getBestRoundTime() == null )
                    currentLapTime = MAX_VALUE; else currentLapTime = r.getBestRoundTime();
                if ( currentCompleteRounds > bestCompletedRoundsResult ) {
                    // this is a best result, clean all previous and stoe it
                    results.removeAllElements();
                    results.add(id);
                    bestCompletedRoundsResult = currentCompleteRounds;
                    bestLapTime = currentLapTime;
                    continue;
                } else if ( currentCompleteRounds == bestCompletedRoundsResult ) {
                    if ( currentLapTime < bestLapTime) {
                        results.removeAllElements();
                        results.add(id);
                        bestCompletedRoundsResult = currentCompleteRounds;
                        bestLapTime = r.getBestRoundTime();
                    }
                } else if ( currentCompleteRounds == bestCompletedRoundsResult 
                        && currentLapTime == bestLapTime && r.getPosition()==MAX_POSITION) {
                    // the same best result; add this to list
                    results.add(id);
                }

            }
            return results;

    }


    private void updatePositions() {
        RaceResult r;
        String id;
        
        // clean all positions to recalculate
        Enumeration<String> one = resultsHash.keys();
        while (one.hasMoreElements()) {
            id = one.nextElement();
            r = resultsHash.get(id);
            r.setPosition(MAX_POSITION);
            resultsHash.put(id, r);
        }
        
        // iterates results list
        for ( int y=0;y<resultsHash.size();y++ ) {

            Vector<String> bestResultRows= getBestResult();

            if ( bestResultRows.size() == 0)
                continue;

            Iterator<String> it = bestResultRows.iterator();
            while(it.hasNext()) {
                id = it.next();
                r = resultsHash.get(id);
                r.setPosition(y+1);
                resultsHash.put(id, r);
              //results[it.next()][1] = new Integer(y+1).toString();
            }
        }

        
    }

  
    public void saveResults(EntityManager entityManager) {
        DBHelper.saveResults(entityManager, raceId, categoryId, groups, heatOrRound, resultsHash);
                
    }

    public String getDescription() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    

    private boolean isThisRoundFinals() {
        if ( heatOrRound[0] != 0)
            return false;
        else
            return true;
    }
} 
