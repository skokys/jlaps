/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.calculator;

import net.jlaps.utils.tableutils.RaceSelection;
import net.jlaps.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.jlaps.readers.AMBReaderRc3;
import net.jlaps.utils.audio.AudioManagerPlayer;
import org.jdesktop.application.ResourceMap;

/**
 *
 * @author lskokan
 */
public class SimpleRaceRunner implements Runnable {

    private JLapsView view;
    private ResourceMap m;
    private Long raceStartTime;
    private Long delay;
    private Integer raceDurationMins;
    private Integer raceAnnMins;
    private boolean raceRunning;
    private Integer postStopTimeSecs;
    private boolean isFinal;
    private long startDelay;
    private Long raceId;
    private long currentCategoryId;
    private String grous;
    private int[] currentHeatOrRound;
    private AMBReaderRc3 r;
    private AudioManagerPlayer player;
    private SimpleResultsCalculator calcul;

    public SimpleRaceRunner(JLapsView v, ResourceMap m, Long startDelay,
            Integer raceDurationMins, Integer raceAnnMins, Integer postStopTime, boolean isFinal,
            Long raceId, RaceSelection race, AMBReaderRc3 r, SimpleResultsCalculator calcul) {

        view = v;
        this.m = m;
        this.delay = startDelay;
        this.raceDurationMins = raceDurationMins;
        this.raceAnnMins = raceAnnMins;
        this.calcul = calcul;
        if (postStopTime == null) {
            this.postStopTimeSecs = 0;
        } else {
            this.postStopTimeSecs = postStopTime;
        }
        this.isFinal = isFinal;
        this.startDelay = startDelay;
        this.raceId = raceId;
        this.currentCategoryId = race.getCategory().getId();
        this.grous = race.getGroups();
        this.currentHeatOrRound = race.getHeatsOrFinals();
        this.r = r;
    }

    public void setRaceRunning(boolean running) {
        this.raceRunning = running;
    }

    public void run() {

        view.runRaceProgressBar((int) startDelay / 1000);
        long sleepStartTime = System.currentTimeMillis();
        try {
            while (raceRunning) {
                Thread.sleep(1000);
                say("bep");
                if ((System.currentTimeMillis() - sleepStartTime) > startDelay) {
                    break;
                }
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(SimpleRaceRunner.class.getName()).log(Level.SEVERE, null, ex);
        }
        say("beep");
        raceStartTime = System.currentTimeMillis();

        r.setRaceStart(raceId, currentCategoryId, grous, currentHeatOrRound);
        calcul.setRaceStartTime(System.currentTimeMillis());
        view.runRaceProgressBar(raceDurationMins * 60);
        while (raceRunning) {
            try {
                view.setStatusLineText(m.getString("RaceDuration") + getThreadRunTime());

                Long raceDuration = System.currentTimeMillis() - raceStartTime;

                if (raceDuration > raceDurationMins * 60 * 1000) {
                    break;
                }
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(SimpleRaceRunner.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        say("bep");

        view.setIsPostTime();

        // wait until end of heat stop
        Long raceEndTime = System.currentTimeMillis();
        while ((raceEndTime + postStopTimeSecs * 1000) > System.currentTimeMillis() && raceRunning) {
            view.setStatusLineText(m.getString("RaceDuration") + getThreadRunTime());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(SimpleRaceRunner.class.getName()).log(Level.SEVERE, null, ex);
            }

        }


        System.out.println("!!!! stopping the hrace");
        view.stopRace(true);
        view.stopRaceProgressBar();
        r.stopRace();
        String msg = m.getString("RaceFinished");
        view.setStatusLineText(msg);
        say("beep");
        say("beep");
        say("beep");
    }

    private String getThreadRunTime() {
        Date d1 = new Date(raceStartTime);
        Date d2 = new Date(System.currentTimeMillis());
        Long secs = new Long((d2.getTime() - d1.getTime()) / 1000);
        Long m = new Long((long) 0), s;
        if (secs < 60) {
            s = secs;
        } else {
            s = secs % 60;
            m = secs / 60;
        }

        if (s < 10) {
            return m + ":0" + s;
        } else {
            return m + ":" + s;
        }
    }

    private void say(String string) {
        player = new AudioManagerPlayer(string);
        try {
            ((Thread) player).start();
        } catch (Exception x) {
            System.out.println("did not say " + string + " because of " + x);
        } finally {
            player = null;
        }

    }
}
