/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author skoky
 */
@Entity
@Table(name="GROUPPED_DRIVERS")
@NamedQueries({@NamedQuery(name = "GrouppedDrivers.deleteForRace", query = "DELETE FROM GrouppedDrivers g WHERE g.raceId = :race_id and g.categoryId = :category_id and g.groups = :groups"),
    @NamedQuery(name="GrouppedDrivers.selectByRound",query="SELECT g from GrouppedDrivers g WHERE raceId = :race_id AND categoryId = :category_id AND groups = :groups"),
    @NamedQuery(name="GrouppedDrivers.selectByRace", query="SELECT g from GrouppedDrivers g WHERE raceId = :race_id ORDER BY  raceId, categoryId, groups, startingPosition")
})
public class GrouppedDrivers implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long raceId;
    private long categoryId;

    @Column(name="CATEGORY_ID")
    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    @Column(name="RACE_ID")
    public Long getRaceId() {
        return raceId;
    }

    public void setRaceId(Long raceId) {
        this.raceId = raceId;
    }
    
    private Integer startingPosition;

    @Column(name="STARTING_POSITION")
    public Integer getStartingPosition() {
        return startingPosition;
    }

    public void setStartingPosition(Integer startingPosition) {
        this.startingPosition = startingPosition;
    }
    
    private String groups;

    @Column(name="DRIVER_ID")
    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    @Column(name="GROUPS")
    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    @Column(name="MODEL_ID")
    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }
    private Long driverId;
    private Long modelId;
           
    
    
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrouppedDrivers)) {
            return false;
        }
        GrouppedDrivers other = (GrouppedDrivers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "net.jlaps.entity.GrouppedDrivers[id=" + id + "]";
    }

}
