/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.entity.helpers;

import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author lskokan
 */
public class RoundsComboBoxModel implements ComboBoxModel {

    private List rounds;
    private Object selectedRound;
    public RoundsComboBoxModel(List list) {
        if ( list == null )
            rounds = new ArrayList();
        else
            rounds = list;
        
    }
    
    public void setSelectedItem(Object arg0) {
        selectedRound = arg0;
        
    }

    public Object getSelectedItem() {
       return selectedRound;
    }

    public int getSize() {
        return rounds.size();
    }

    public Object getElementAt(int arg0) {
        return rounds.get(arg0);
    }

    public void addListDataListener(ListDataListener arg0) {
        
    }

    public void removeListDataListener(ListDataListener arg0) {
        
    }

}
