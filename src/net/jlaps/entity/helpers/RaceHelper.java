/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.entity.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import net.jlaps.entity.Category;
import net.jlaps.entity.DetailResult;
import net.jlaps.entity.Driver;
import net.jlaps.entity.DriversAndIds;
import net.jlaps.entity.Event;
import net.jlaps.entity.GrouppedDrivers;
import net.jlaps.entity.Race;
import net.jlaps.utils.DBHelper;
import net.jlaps.utils.tableutils.RaceSelection;

/**
 *
 * @author lskokan
 */
public class RaceHelper {

    private static EntityManager entityManager;
    

    static {
        entityManager = javax.persistence.Persistence.createEntityManagerFactory(DBHelper.getPMName()).createEntityManager();
    }

    public static List createDetailResultsList(List<Event> eventList, Integer minLapTime) {
        Iterator<Event> it = eventList.iterator();
        Vector<String> allIDs = new Vector<String>();
        String id;

        ArrayList<DetailResult> results = new ArrayList<DetailResult>();

        while (it.hasNext()) {
            id = it.next().getIdentificationId();
            if (!allIDs.contains(id.trim())) {
                allIDs.add(id);
            }
        }

        List<Event> newEventList = new ArrayList<Event>(eventList);
//        Collections.copy(newEventList, eventList);
        Collections.sort(newEventList, new Comparator() {

            public int compare(Object o1, Object o2) {
                Event e1 = (Event) o1;
                Event e2 = (Event) o2;
                if (e1.getTimestamp().equals(e2.getTimestamp())) {
                    return 0;
                }
                if (e1.getTimestamp().after(e2.getTimestamp())) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
        Enumeration<String> e = allIDs.elements();

        while (e.hasMoreElements()) {
            String currentId = e.nextElement();
            it = newEventList.iterator();
            Date lastDate = null;
            long bestTime = 0;
            DetailResult result;
            result = new DetailResult();
            while (it.hasNext()) {
                Event ex = it.next();

                if (ex.getIdentificationId().equalsIgnoreCase(currentId)) {
                    result.setDriverName(currentId);

                    if (lastDate != null) {
                        bestTime = (ex.getTimestamp().getTime() - lastDate.getTime());
//                    if (bestTime < minLapTime) {
//                        continue;
                    //                   }
                    }
                    if (lastDate != null && bestTime > minLapTime * 1000) {
                        result.addResult(new Float(bestTime) / 1000);
                        lastDate = ex.getTimestamp();
                    } else if (lastDate == null) {
                        lastDate = ex.getTimestamp();
                    }
                }
            }
            results.add(result);

        }

        return results;
    }

    public static ArrayList createInitialListofDrivers(Race todaysRace, EntityManager em) {
        ArrayList driversList = new ArrayList();
        List<GrouppedDrivers> gds = DBHelper.getAllGrouppedDriverIdsForRace(todaysRace, em);

        Iterator<GrouppedDrivers> i = gds.iterator();
        while (i.hasNext()) {
            GrouppedDrivers gd = i.next();
            
            DriversAndIds d = new DriversAndIds();
            d.setRaceName(todaysRace.getName());
            d.setCategoryName(DBHelper.getCategoryById(gd.getCategoryId(),em).getName());
            d.setGroup(gd.getGroups());
            Driver dr = DBHelper.getDriverById(gd.getDriverId(),em);
            d.setDriverName(dr.getFirstName() + " " + dr.getSecondName());
            d.setIdentificationId(DBHelper.getModelsIdentId(gd.getModelId(),em));
            driversList.add(d);
        }
        return driversList;
    }

    /**
     * Converts categories string (01;3;5;4585) to ids in Category table
     * 
     * @param categories - string of IDs. Sample: 334;33;32;3;2344
     * @return list of ids
     */
    public static List getCategoriesByIDs(String categories) {
        List cats = new ArrayList();
        String oneCategryId;
        int category = 0;

        if (categories == null) {
            return new ArrayList();
        }
        if (categories.length() == 0) {
            return new ArrayList();
        }

        try {
            StringTokenizer st = new StringTokenizer(categories, ";");
            while (st.hasMoreTokens()) {
                oneCategryId = st.nextToken();

                try {
                    category = new Integer(oneCategryId).intValue();
                } catch (RuntimeException x) {
                    System.out.println("Invalid category value:" + categories);
                    return new ArrayList();
                }

                Category c = findCategoryById(category);
                cats.add(c);

            }
        } catch (Exception e) {
            System.err.println("Exception here " + e);
        }
        return cats;
    }

    public static int getCurrentGroupCount(Race todaysRace, long currentCategoryId) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public static int getNumOfGroupsForRaceAndCat(Race todaysRace, long currentCategoryId) {
        StringTokenizer stCats = new StringTokenizer(todaysRace.getCategories(), ";");
        StringTokenizer stGroups = new StringTokenizer(todaysRace.getGroups(), ";");
        String oneCat, oneGroup;






        while (stCats.hasMoreElements()) {
            oneCat = stCats.nextToken();
            oneGroup = stGroups.nextToken();
            if (new Long(oneCat) == currentCategoryId) {
                return new Integer(oneGroup);
            }
        }
        return 0;
    }

    public static List getRoundsListStrings(Hashtable<Category, List> catGroups) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /*    public static List<Round> getRoundsListStrings(Hashtable<Category, List> catGroups) {
    if (catGroups == null) {
    return null;
    }
    Enumeration<Category> e = catGroups.keys();
    List<Round> result = new ArrayList<Round>();
    List<String> groups;
    Category c;
    
    while (e.hasMoreElements()) {
    c = e.nextElement();
    groups = catGroups.get(c);
    
    Iterator<String> it = groups.iterator();
    String group;
    while (it.hasNext()) {
    group = it.next();
    
    
    for (int y = 1; y <= c.getHeats(); y++) {
    result.add(new Round(c, group, y, 0));
    }
    for (int y = 1; y <= c.getFinals(); y++) {
    result.add(new Round(c, group, 0, y));
    }
    
    }
    }
    return result;
    }
     */
    public static String updateGroupsNumberForRaceCats(Race todaysRace, long currentCategoryId, Integer newGroupsCount) {
        StringTokenizer stCats = new StringTokenizer(todaysRace.getCategories(), ";");
        StringTokenizer stOrgGroups = new StringTokenizer(todaysRace.getGroups(), ";");

        String catId, orgGroups;
        String newGroups = null;
        while (stCats.hasMoreElements()) {
            catId = stCats.nextToken();
            orgGroups = stOrgGroups.nextToken();
            if (currentCategoryId == new Long(catId) && newGroups == null) {
                newGroups = newGroupsCount.toString();
            } else if (currentCategoryId == new Long(catId) && newGroups != null) {
                newGroups += ";" + newGroupsCount.toString();
            } else if (currentCategoryId != new Long(catId) && newGroups == null) {
                newGroups = orgGroups;
            } else if (currentCategoryId != new Long(catId) && newGroups != null) {
                newGroups += ";" + orgGroups;
            }
        }

        if (entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().rollback();
        }

        todaysRace.setGroups(newGroups);
        Query q = entityManager.createNamedQuery("Race.findById");
        List<Race> dbRaces = q.setParameter("id", todaysRace.getId()).getResultList();
        Race dbRace = dbRaces.get(0);
        dbRace.setGroups(todaysRace.getGroups());

        entityManager.getTransaction().begin();
        entityManager.persist(dbRace);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();

        return newGroups;
    }

    private static Category findCategoryById(int category) {
        Long id = new Long(new Integer(category).longValue());
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.flush();
        List<Category> resultAll = entityManager.createNamedQuery("Category.all").getResultList();
        System.out.println("All cats:" + resultAll);

        List<Category> result = entityManager.createNamedQuery("Category.findById").setParameter("id", id).getResultList();
        if (result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }

    /**
     * for each category gets list of groups like A, B , C in a List
     * @param categories list
     * @param goupsString string list separated by ;
     * @return hastable of category groups pairs
     */
    public static Hashtable<Category, List> getRoundsList(List<Category> categories, String goupsString) {

        if (goupsString == null) {
            return null;
        }
        Hashtable<Category, List> map = new Hashtable<Category, List>();

        StringTokenizer st = new StringTokenizer(goupsString, ";");
        char groupName = 'A';
        String oneGroup;
        int oneGroupNum;
        List<String> oneCatGroups;

        Iterator<Category> catIter = categories.iterator();
        Category oneCat;
        while (catIter.hasNext()) {
            oneCat = catIter.next();

            oneGroup = st.nextToken();
            oneCatGroups = new ArrayList<String>();

            try {
                oneGroupNum = new Integer(oneGroup).intValue();
            } catch (NumberFormatException ex) {
                break;
            }

            for (int g = 0; g < oneGroupNum; g++) {
                oneCatGroups.add(new String((new char[]{(char) (groupName + g)})));
            }

            if (oneCat != null) {
                map.put(oneCat, oneCatGroups);
            }
        }

        return map;

    }
}
