/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.entity.helpers;

import java.util.Comparator;
import net.jlaps.entity.RaceResult;

/**
 *
 * @author lskokan
 */
public class ResultsComparator implements Comparator {

    private boolean afterFinish;

    public ResultsComparator(boolean afterFinish) {
        this.afterFinish = afterFinish;
    }

    public int compare(Object arg0, Object arg1) {
        
        if ( arg0 == null || arg1 == null )
            return 0;
        
        RaceResult r1 = (RaceResult) arg0;
        RaceResult r2 = (RaceResult) arg1;

        
        if (afterFinish)  { // compare by rounds and time
            
                if ( r1.getCompleteRoundsAchieved() > r2.getCompleteRoundsAchieved())
                    return -1;
                else if (r1.getCompleteRoundsAchieved() < r2.getCompleteRoundsAchieved())
                    return 1;
                else {
                    if ( r1.getFinishedTime() == null )
                        return -1;
                    if ( r2.getFinishedTime() == null )
                        return 1;
                    if ( r1.getFinishedTime() > r2.getFinishedTime())
                        return 1;
                    else if ( r1.getFinishedTime() < r2.getFinishedTime())
                        return -1;
                    else
                        return 0;
                }
            
        } else { // compare by rounds and best round time

            if ( r1.getCompleteRoundsAchieved() > r2.getCompleteRoundsAchieved())
                return -1;
                else if (r1.getCompleteRoundsAchieved() < r2.getCompleteRoundsAchieved())
                    return 1;
                else {
                    if ( r1.getBestRoundTime() == null )
                        return -1;
                    if ( r2.getBestRoundTime() == null )
                        return 1;
                    if ( r1.getBestRoundTime() > r2.getBestRoundTime())
                        return 1;
                    else if ( r1.getBestRoundTime() < r2.getBestRoundTime())
                        return -1;
                    else 
                        return 0;
                
                }
        
        }
    }
}


