/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.entity;


import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author skoky
 */
@Entity
@Table(name = "MODEL")
@NamedQueries({@NamedQuery(name = "Model.findById", query = "SELECT m FROM Model m WHERE m.id = :id"),    @NamedQuery(name = "Model.findByDriverId", query = "SELECT m FROM Model m WHERE m.driverId = :driverId"),     @NamedQuery(name = "Model.findByModelName", query = "SELECT m FROM Model m WHERE m.modelName = :modelName"),     @NamedQuery(name = "Model.findByFrequency1", query = "SELECT m FROM Model m WHERE m.frequency1 = :frequency1"),     @NamedQuery(name = "Model.findByIdentificationType", query = "SELECT m FROM Model m WHERE m.identificationType = :identificationType"),     @NamedQuery(name = "Model.findByIdentificationIdAndCategoryId",         query = "SELECT m FROM Model m WHERE m.identificationId = :identification_id AND m.category = :category_id")})
public class Model implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "MODEL_NAME")
    private String modelName;
    @Column(name = "FREQUENCY1")
    private String frequency1;
    @Column(name = "IDENTIFICATION_TYPE")
    private String identificationType;
    @Column(name = "IDENTIFICATION_ID")
    private String identificationId;
    
    @Column(name="CATEGORY_ID")
    private Long category;

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long categories) {
        this.category = new Long(categories);
    }
    
    @Column(name="DRIVER_ID")
    private Long driverId;
    
    
    

    public Model() {
    }

    public Model(String modelName,String frequency1, String identificationType, String identificationId, Long categoryId ) {
        this.modelName = modelName;
        this.frequency1 = frequency1;
        this.identificationType = identificationType;
        this.identificationId = identificationId;
        this.category = categoryId;
    }
    
    public Model(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Long oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        String oldModelName = this.modelName;
        this.modelName = modelName;
        changeSupport.firePropertyChange("modelName", oldModelName, modelName);
    }

    public String getFrequency1() {
        return frequency1;
    }

    public void setFrequency1(String frequency1) {
        String oldFrequency1 = this.frequency1;
        this.frequency1 = frequency1;
        changeSupport.firePropertyChange("frequency1", oldFrequency1, frequency1);
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        String oldIdentificationType = this.identificationType;
        this.identificationType = identificationType;
        changeSupport.firePropertyChange("identificationType", oldIdentificationType, identificationType);
    }

    public String getIdentificationId() {
        return identificationId;
    }

    public void setIdentificationId(String identificationId) {
        String oldIdentificationId = this.identificationId;
        this.identificationId = identificationId;
        changeSupport.firePropertyChange("identificationId", oldIdentificationId, identificationId);
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        Long oldDriverId = this.driverId;
        this.driverId = driverId;
        changeSupport.firePropertyChange("driverId", oldDriverId, driverId);
    }
    

            

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Model)) {
            return false;
        }
        Model other = (Model) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model[id=" + id + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

}
