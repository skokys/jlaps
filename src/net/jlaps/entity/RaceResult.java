/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author lskokan
 */
@Entity
@Table(name="RACE_RESULT")
// @NamedQueries({@NamedQuery(name = "Result.findByRace", query = "SELECT r FROM Result r WHERE r.race_id = :raceid and r.category_id = :categoryid and final_number = :finalnumber and heat_number = :heatnumber")})
@NamedQueries({@NamedQuery(name="RaceResult.findByRound",query="SELECT r FROM RaceResult r WHERE r.raceId = :race_id and r.categoryId = :category_id and r.groups = :groups and r.finalNumber = :final_number and r.heatNumber= :heat_number"),
// @NamedQuery(name="RaceResults.progress.heat",query="SELECT r.driverName, max(r.completeRoundsAchieved), min(r.bestRoundTime) FROM RaceResult r WHERE r.raceId = :race_id and r.categoryId = :category_id and r.finalNumber = 0 group by driverName ORDER BY 2 desc, 3 asc"),
@NamedQuery(name="RaceResults.progress.heat",query="SELECT r FROM RaceResult r WHERE r.raceId = :race_id and r.categoryId = :category_id and r.finalNumber = 0 ORDER BY r.position"),
@NamedQuery(name="RaceResults.progress.final",query="SELECT r FROM RaceResult r WHERE r.raceId = :race_id and r.categoryId = :category_id and r.heatNumber = 0 ORDER BY r.position")})
public class RaceResult implements Serializable { 
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)    
    private Long id;

    @Column(name = "DRIVER_NAME")
    private String driverName;


    @Column(name = "RACE_ID")
    private Long raceId;


    @Column(name = "CATEGORY_ID")
    private Long categoryId;



    @Column(name = "GROUPS")
    private String groups;


    @Column(name = "HEAT_NUMBER")
    private Integer heatNumber;


    @Column(name = "FINAL_NUMBER")
    private Integer finalNumber;


    @Column(name = "COMPLETE_ROUNDS_ACHIEVED")
    private Integer completeRoundsAchieved;




    @Column(name = "POSITION")
    private Integer position; 



    @Column(name = "STARTING_POSITION")
    private Integer startingPosition;
    /*
    @Column(name="POINTS")
    public Integer getPoints() {
    return points;
    }
    public void setPoints(Integer points) {
    this.points = points;
    }
     */

    @Column(name = "BEST_ROUND_TIME")
    private Float bestRoundTime;
//    private Integer points;
    
    private Boolean isFinished;


    @Column(name = "FINISHED_TIME")
    private Long finishedTime;
    
    @Column(name = "START_TIME")
    private Long startTime;

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
    
    
    
    public Long getFinishedTime() {
        return finishedTime;
    }
    
    public void setFinishTime(Long finishedTime) {
        this.finishedTime = finishedTime;
    }
    public Boolean getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(Boolean isFinished) {
        this.isFinished = isFinished;
    }
    
    public RaceResult(Long raceId, Long categoryId, String groups, int[] heatOrFinal) {
        this.raceId = raceId;
        this.categoryId = categoryId;
        this.groups = groups;
        this.heatNumber = heatOrFinal[0];
        this.finalNumber = heatOrFinal[1];
    }

    public RaceResult() {
        
    }
    /*
    @Column(name="POINTS")
    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }
    */
    
    public Float getBestRoundTime() {
        return bestRoundTime;
    }

    public void setBestRoundTime(Float bestRoundTime) {
        this.bestRoundTime = bestRoundTime;
    }
    

    public Integer getStartingPosition() {
        return startingPosition;
    }

    public void setStartingPosition(Integer startingPosition) {
        this.startingPosition = startingPosition;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getCompleteRoundsAchieved() {
        return completeRoundsAchieved;
    }

    
    public void setCompleteRoundsAchieved(Integer completeRoundsAchieved) {
        this.completeRoundsAchieved = completeRoundsAchieved;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public Integer getFinalNumber() {
        return finalNumber;
    }

    public void setFinalNumber(Integer finalNumber) {
        this.finalNumber = finalNumber;
    }

    public Integer getHeatNumber() {
        return heatNumber;
    }

    public void setHeatNumber(Integer heatNumber) {
        this.heatNumber = heatNumber;
    }

    public Long getRaceId() {
        return raceId;
    }

    public void setRaceId(Long raceId) {
        this.raceId = raceId;
    }



    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
    
         
    public Long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RaceResult)) {
            return false;
        }
        RaceResult other = (RaceResult) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Result[id=" + id + "]";
    }


    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }    
}
