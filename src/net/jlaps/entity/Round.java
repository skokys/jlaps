/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.entity;

/**
 *
 * @author lskokan
 */
public class Round {
    private String groups;

    
    public Round(Category c, String g, int heatIn, int finalIn) {
        this.category = c;
        this.groups = g;
        this.heatNumber = heatIn;
        this.finalNumber = finalIn;
    }
    
    @Override
    public String toString() {
        if ( heatNumber != 0 )
            return category.getName() + ":" + groups +  " (heat:" + heatNumber + ")";
        if ( finalNumber != 0)
            return category.getName() + ":" + groups +  " (final:" + finalNumber + ")";
        return "uniniciated roufn object";
        
    }
    private Category category;
    
    private int heatNumber=0;
    private int finalNumber=0;

    public Category getCategory() {
        return category;
    }

    public String getGroups() {
        return groups;
    }
    
    public void setCategory(Category category) {
        this.category = category;
    }

    public int getFinalNumber() {
        return finalNumber;
    }

    public void setFinalNumber(int finalNumber) {
        this.finalNumber = finalNumber;
    }

    public int getHeatNumber() {
        return heatNumber;
    }

    public void setHeatNumber(int heatNumber) {
        this.heatNumber = heatNumber;
    }

    
    
    

}
