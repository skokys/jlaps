/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.entity; 

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author lskokan
 */
@Entity
@Table(name = "CATEGORY")
@NamedQueries({@NamedQuery(name = "Category.findById", query = "SELECT c FROM Category c WHERE c.id = :id"), 
@NamedQuery(name="Category.all", query="SELECT c from Category c"), 
@NamedQuery(name = "Category.findByName", query = "SELECT c FROM Category c WHERE c.name = :name"), 
@NamedQuery(name = "Category.findByFinals", query = "SELECT c FROM Category c WHERE c.finals = :finals"), 
@NamedQuery(name = "Category.findByHeats", query = "SELECT c FROM Category c WHERE c.heats = :heats")})
public class Category implements Serializable { 
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "FINALS")
    private Integer finals;
    @Column(name = "HEATS")
    private Integer heats;
    
    @Column(name="BEST_HEATS")
    private Integer bestHeats;
    
    @Column(name="BEST_FINALS")
    private Integer bestFinals;

    public Integer getBestFinals() {
        return bestFinals;
    }

    public void setBestFinals(Integer bestFinals) {
        this.bestFinals = bestFinals;
    }

    public Integer getBestHeats() {
        return bestHeats;
    }

    public void setBestHeats(Integer bestHeats) {
        this.bestHeats = bestHeats;
    }
    
    @Column(name="CalculatorName")
    private String CalculatorName;

    public String getCalculatorName() {
        return CalculatorName;
    }

    public void setCalculatorName(String CalculatorName) {
        this.CalculatorName = CalculatorName;
    }

    public Category() {
    }
    
    public Category(String name, Integer heats, Integer finals, Integer bestHeats, 
            Integer bestFinals) {
        this.name = name;
        this.heats = heats;
        this.finals = finals;
        this.bestHeats = bestHeats;
        this.bestFinals = bestFinals;
    }

    public Category(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFinals() {
        return finals;
    }

    public void setFinals(Integer finals) {
        this.finals = finals;
    }

    public Integer getHeats() {
        return heats;
    }

    public void setHeats(Integer heats) {
        this.heats = heats;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Category)) {
            return false;
        }
        Category other = (Category) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        if ( name == null )
            return "";
        
        return  name +  " (" + heats + "/" + finals + ")";
    }

}
