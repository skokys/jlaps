/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.entity;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author skoky
 */
@Entity
@Table(name = "DEVICE_EVENT")

@NamedQueries({@NamedQuery(name="eventQuery.query", query="SELECT e FROM Event e"),
    @NamedQuery(name = "Event.findById", query = "SELECT e FROM Event e WHERE e.id = :id"), 
        @NamedQuery(name = "Event.findByIdentificationType", query = "SELECT e FROM Event e WHERE e.identificationType = :identificationType"), 
        @NamedQuery(name = "Event.findByIdentificationId", query = "SELECT e FROM Event e WHERE e.identificationId = :identificationId")} )
public class Event implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "IDENTIFICATION_TYPE")
    private String identificationType;
    @Column(name = "IDENTIFICATION_ID")
    private String identificationId;

    private Long lapTimeStamp;
    
    @Column(name="EVENT_TIMESTAMP")
    private Date eventTimestamp;

    public Date getTimestamp() {
        return eventTimestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.eventTimestamp = timestamp;
    }
    
    public Event() {
    }

    public Event(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Long oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        String oldIdentificationType = this.identificationType;
        this.identificationType = identificationType;
        changeSupport.firePropertyChange("identificationType", oldIdentificationType, identificationType);
    }

    public String getIdentificationId() {
        return identificationId;
    }

    public void setIdentificationId(String identificationId) {
        String oldIdentificationId = this.identificationId;
        this.identificationId = identificationId;
        changeSupport.firePropertyChange("identificationId", oldIdentificationId, identificationId);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Event)) {
            return false;
        }
        Event other = (Event) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Event[id=" + id + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    @Column(name="RACE_ID")
    private Long raceId;
    
    @Column(name="HEAT_NUMBER")
    private Integer heatNumber;
    
    @Column(name="FINAL_NUMBER")
    private Integer finalNumber;

    public Long getRaceId() {
        return raceId;
    } 

    public void setRaceId(Long raceId) {
        this.raceId = raceId;
    }

    public Integer getFinalNumber() {
        return finalNumber;
    }

    public void setFinalNumber(Integer finalNumber) {
        this.finalNumber = finalNumber;
    }

    public Integer getHeatNumber() {
        return heatNumber;
    }

    public void setHeatNumber(Integer heatNumber) {
        this.heatNumber = heatNumber;
    }
    

    @Column(name="CATEGORY_ID")
    private Long categoryId;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
    
    @Column(name="GROUPS")
    private String groups;

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public Long getLapTimeTemp() {
        return lapTimeStamp;
    }

    public void setLapTimeTemp(Long lapTimeStamp) {
        this.lapTimeStamp = lapTimeStamp;
    }
    
    
}
