/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.entity;

/**
 *
 * @author lskokan
 */
public class DetailResult {
    private String driverName;
    private String resultsString ="";

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getResultsString() {
        return resultsString;
    }

    public void setResultsString(String resultsString) {
        this.resultsString = resultsString;
    }

       
    public void addResult(Float result) {
        resultsString += " " + result;
    }
}
