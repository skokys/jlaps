/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.entity;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author skoky
 */
@Entity
@Table(name = "RACE")
@NamedQueries({@NamedQuery(name = "Race.findById", query = "SELECT r FROM Race r WHERE r.id = :id"), @NamedQuery(name = "Race.findByName", query = "SELECT r FROM Race r WHERE r.name = :name"), @NamedQuery(name = "Race.findByProposition", query = "SELECT r FROM Race r WHERE r.proposition = :proposition"), @NamedQuery(name = "Race.findByDate", query = "SELECT r FROM Race r WHERE r.raceDate = :raceDate"),
                @NamedQuery(name="Race.all",query = "SELECT r FROM Race r")})
public class Race implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "NAME", nullable = false)
    private String name;
    @Column(name = "PROPOSITION", nullable = true)
    private String proposition;
    @Column(name = "RACE_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date raceDate; 

    @Column(name = "CATEGORIES")
    private String categories;
    
    @Column (name="GROUPS")
    private String groups;
    
    @Column (name="MIN_LAP_TIME")
    private Integer minLapTime;

    public Integer getMinLapTime() {
        return minLapTime;
    }

    public void setMinLapTime(Integer minLapTime) {
        this.minLapTime = minLapTime;
    }
    

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }
    

    public Race() {
        setName("");
        setRaceDate(new Date());
                
    }

    public Race(Long id) {
        this.id = id;
    }

    public Race(String name, String proposition, Date raceDate, Integer minLapTime, 
            String categories, String groups) {
        this.name = name;
        this.proposition = proposition;
        this.raceDate = raceDate;
        this.minLapTime = minLapTime;
        this.categories = categories;
        this.groups = groups;
        

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Long oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public String getProposition() {
        return proposition;
    }

    public void setProposition(String proposition) {
        String oldProposition = this.proposition;
        this.proposition = proposition;
        changeSupport.firePropertyChange("proposition", oldProposition, proposition);
    }

    public Date getRaceDate() {
        return raceDate;
    }

    public void setRaceDate(Date raceDate) {
        Date oldDate = this.raceDate;
        this.raceDate = raceDate;
        changeSupport.firePropertyChange("raceDate", oldDate, raceDate);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Race)) {
            return false;
        }
        Race other = (Race) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Race " + name + " [id=" + id + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

}
