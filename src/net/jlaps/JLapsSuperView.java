/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps;

import org.jdesktop.application.FrameView;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;

/**
 *
 * @author lskokan
 */
public class JLapsSuperView extends FrameView {

    /// tbd replace
    ResourceMap rm = getResourceMap();
    boolean saveNeeded;
    
    public JLapsSuperView(SingleFrameApplication app) {
        super(app);
    }
    
    

    public void setSaveNeeded(boolean saveNeeded) {
        if (saveNeeded != this.saveNeeded) {
            this.saveNeeded = saveNeeded;
            firePropertyChange("saveNeeded", !saveNeeded, saveNeeded);
        }
    }
    
    public boolean isSaveNeeded() {
        return saveNeeded;
    }


    
    
    public String msg(String msg) {
        String m = rm.getString(msg);
        
        if (m == null) {
            System.out.println("Missing resource for key: " + msg + " in:" + rm.getBundleNames().get(0));
            System.exit(1);
        }
        return m;
    }    
}
