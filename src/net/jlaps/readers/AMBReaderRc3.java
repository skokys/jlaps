package net.jlaps.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import net.jlaps.DeviceConsole;
import net.jlaps.ViewScreen;


public class AMBReaderRc3 implements Runnable  {

    Logger log = Logger.getLogger(this.getClass().toString());
    
    String ambhost;
    int ambport;
    
    private static long  lastEventReceived;

    int readDelayMs = 100;

    Socket s = null;
    BufferedReader in = null;
    String data;

    static char[] delim = new char[] {9};
  
    StoreManager sm;
    
    private char[] transponderid = new char[7];
    private long timestamp;
    private long raceId;
    private long categoryId; 
    private String groups;
    private int[] round;

    private EntityManager e;
    private ViewScreen v;
    private DeviceConsole dc; 
    

    public AMBReaderRc3(String ip, String port, EntityManager e, ViewScreen view, 
            DeviceConsole dc) {
        this.ambhost = ip;
    
        this.ambport = new Integer(port);
        this.e = e;
        this.v = view;
        this.dc = dc;
        sm = new StoreManager(e, v);
        log.setLevel(Level.WARNING);
        log.info("AMB device reader initiated" );
    }

    public void setRaceStart(long raceId, long categoryId, String groups, int[] round) {
        log.info("Setting race start");
        this.raceId = raceId;
        this.categoryId = categoryId;
        this.groups = groups;
        this.round = round;
    }

    public void stopRace() {
        log.info("Setting race stop");
        this.raceId = 0;
        this.categoryId = 0;
        this.groups = null;
        this.round = null;
    }



    private void parseData() {
        log.info("Parsing received data:" + data);
        StringTokenizer st = new StringTokenizer(data,new String(delim));
        st.nextToken();
        st.nextToken();
        st.nextToken();
        transponderid = st.nextToken().toCharArray();
        log.info("Info recevied for transponder id:" + new String(transponderid));
//        log("Tnasp. id:" + new String(transponderid));
        
                
    }
    
    public static long getLastEventMillis() {
        return lastEventReceived;
    }


    private void reconnectAMB() throws IOException {
        v.setAmbStatus("Connectioning AMB...");
        log.info("Connecting to AMB....");
        try {
            if (in != null) in.close();
            if (s != null) s.close();

            s = new Socket(ambhost, ambport);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));

            v.setAmbStatus("AMB Connected");
            log.info("AMB connected");
            
            while (true) {
                data = in.readLine();
                lastEventReceived = System.currentTimeMillis();
                if ( data == null ) {
                    log.info("Received nothing from AMB...");
                    break;
                }
                timestamp = System.currentTimeMillis();
                if ( data.length() > 0)  {
                    log.info("Data received from AMB");
                    dc.addEvent(data.substring(1, data.length()).replace((char)9,"-".charAt(0) ),new Date(timestamp));
                    char[] x = data.toCharArray();
                    if ( data.substring(1,2).equalsIgnoreCase("@")) {
                        log.info("Data receord received");
                        parseData();
                        if ( raceId != 0 && categoryId != 0 && groups != null && round != null )
                            storeEvent();
                        else
                            v.setAmbStatus("AMB Connected");
                        
                    } else
                        log.info("Status event received from AMB");

                    
                }
            }

        } catch (IOException e) {
            v.setAmbStatus("AMB Disconnected.");
            log.info("AMB disconnected, throwing exception");
            throw e;
        } finally {
            try {
                if (in != null) in.close();
                if (s != null) s.close();
            } catch (IOException e) {
                
            }

        }

    }


    public void storeEvent() {
        if ( transponderid == null ) {
            return;
        }

      Runnable r = new Runnable() {
            
            public void run() {
                log.info("Starting new thread for storind data");
                sm.saveEventEM(raceId,categoryId, groups, round, new String(transponderid), timestamp);
            }
        };
            
        new Thread(r).start();
        
    }

    public void run() {
        
        
     while (true)
        try {
            reconnectAMB();
        } catch (IOException e) {
            try {
                log.info("Unable to connect to AMB on " + ambhost + ":" + ambport);
                 Thread.sleep(readDelayMs * 10);
            } catch (InterruptedException ex) {
                log.log(Level.SEVERE, "interrupted", ex);
            }
            
        }

        
    }

    public static String getDeviceAbbrev() {
        return "AMB";
    }
}
