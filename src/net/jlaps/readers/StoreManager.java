package net.jlaps.readers;



import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import net.jlaps.ViewScreen;
import net.jlaps.entity.Event;

/**
 *
 * @author skoky
 */
public class StoreManager {
    
    EntityManager entityManager;
    ViewScreen view;
    String insertString="insert into EVENT (RACE_ID,CATEGORY_ID, HEAT_NUMBER,FINAL_NUMBER,identification_id,identification_type) values (";
    Connection c;
    
    public StoreManager(EntityManager e, ViewScreen v) {
        this.entityManager = e;
        this.view = v;
    }

    public void saveEventEM(long raceId, long categoryId, String groups, int[] round, String identification, long datetime) {
        Event e = new Event();
        e.setCategoryId(categoryId);
        e.setGroups(groups);
        e.setFinalNumber(round[1]);
        e.setHeatNumber(round[0]);
        e.setIdentificationId(identification);
        e.setIdentificationType(AMBReaderRc3.getDeviceAbbrev());
        e.setRaceId(raceId);
        e.setTimestamp(new Date(datetime));
        
        synchronized (this ) {
            if ( entityManager.getTransaction().isActive()) 
                entityManager.getTransaction().rollback();
            entityManager.getTransaction().begin();
            entityManager.persist(e);
            entityManager.getTransaction().commit();
        }

        view.refreshResults(identification,datetime);
        
        
    }
    public void saveEventUnused(long raceId, long categoryId, int[] round, String identification, long datetime) {
        try {

            c.setAutoCommit(true);
            Statement st = c.createStatement();
            String finalInsert = insertString + raceId + "," + categoryId + "," + round[0] + "," + round[1] + ",\"" + identification + "\"" + ",\"AMB\");";
            int result = st.executeUpdate(finalInsert);
        } catch (SQLException ex) {
            Logger.getLogger(StoreManager.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
    
}
