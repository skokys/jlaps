package net.jlaps;

import net.jlaps.utils.tableutils.DateEditor;
import net.jlaps.utils.tableutils.DateRenderer;
import net.jlaps.utils.tableutils.CategoryListModel;
import net.jlaps.utils.audio.AudioManagerRecorder;
import net.jlaps.utils.audio.AudioManagerPlayer;
import net.jlaps.utils.tableutils.ModelsTableModel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.PatternSyntaxException;
import javax.persistence.Query;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import net.jlaps.entity.Category;
import net.jlaps.entity.Driver;
import net.jlaps.entity.Event;
import net.jlaps.entity.Model;
import net.jlaps.entity.Race;
import net.jlaps.entity.helpers.RaceHelper;
import net.jlaps.utils.*;
import org.jdesktop.application.Action;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.beansbinding.AbstractBindingListener;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.PropertyStateEvent;

/**
 * The application's main frame and related functionality
 */
public class RaceSetupView extends JLapsSuperView {

    private AudioManagerRecorder audioManagerRecorder;
    private AudioManagerPlayer audioManagerPlayer;
    private boolean categoryRecordSelected;
    private boolean driversNameRecordingInProgress = false;
    private RaceSetupView.raceTableLlistener rtl = new RaceSetupView.raceTableLlistener();
    private RaceSetupView.categoryListListener cll = new RaceSetupView.categoryListListener();
    private RaceSetupView.modelTableLlistener mtl = new RaceSetupView.modelTableLlistener();
    private JLapsView view;

    public RaceSetupView(SingleFrameApplication app, JLapsView view) {
        super(app);
        this.view = view;

        entityManager = javax.persistence.Persistence.createEntityManagerFactory(DBHelper.getPMName()).createEntityManager();


        initComponents();

        raceTable.getColumnModel().getColumn(1).setCellRenderer(new DateRenderer());
        raceTable.getColumnModel().getColumn(1).setCellEditor(new DateEditor());

       
        enableRaceTableListeners(true);

        JCheckBox x = new JCheckBox();
        jPanel1.add(x);

        modelsTable.setModel(new ModelsTableModel(null, entityManager));
        refreshCatsInModelTable();


        TableModel mttm = masterTable.getModel();
        RowSorter<TableModel> sorter =
                new TableRowSorter<TableModel>(mttm);
        masterTable.setRowSorter(sorter);

        masterTable.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {

                    public void valueChanged(ListSelectionEvent e) {
                        firePropertyChange("recordSelected", !isRecordSelected(), isRecordSelected());
                        refreshModelsTable();
                    }
                });



        modelsTable.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {

                    public void valueChanged(ListSelectionEvent e) {
                        firePropertyChange("detailRecordSelected", !isDetailRecordSelected(), isDetailRecordSelected());
                        int row = modelsTable.getSelectedRow();
                    }
                });


        modelsTable.getModel().addTableModelListener(new TableModelListener() {

            public void tableChanged(TableModelEvent e) {
                saveModelsTable(e.getColumn(), e.getFirstRow());
            }
        });
        // tracking table selection
        raceTable.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {

                    public void valueChanged(ListSelectionEvent e) {
                        firePropertyChange("raceRecordSelected", !isRaceRecordSelected(), isRaceRecordSelected());
                    }
                });

        // tracking table selection
        categoryTable.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {

                    public void valueChanged(ListSelectionEvent e) {
                        firePropertyChange("categoryRecordSelected", !isCategoryRecordSelected(), isCategoryRecordSelected());
                    }
                });


        // tracking changes to save
        bindingGroup.addBindingListener(new AbstractBindingListener() {

            @Override
            public void targetChanged(Binding binding, PropertyStateEvent event) {
                // save action observes saveNeeded property
                setSaveNeeded(true);
            }
        });

        tabbedPane.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                System.out.println("satte changed" + e.getSource());
                JTabbedPane tp = (JTabbedPane) e.getSource();
                refreshTabbs(tp.getSelectedIndex());
            }
        });

        
        refreshCategoryRaceList();
        refresh();
 
                

        // have a transaction started
        entityManager.getTransaction().begin();

    }

    private void refreshCategoryRaceList() {

            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
                entityManager.getTransaction().begin();
            }
        
        List l = DBHelper.getAllCategories(entityManager);
        CategoryListModel clm = new CategoryListModel(l);
        categoryRaceList.setModel(clm);
        
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
                entityManager.getTransaction().begin();
            }
        
    }

    
    private void refreshTabbs(int selectedIndex) {
        System.out.println("Selected index:" + selectedIndex);
        switch (selectedIndex) {
            case 0: // race tan
                    refreshCategoryRaceList();
                    refreshRace();
                break;
            case 1: // categories tab
                    refreshCategory();
                break;
            case 2: //drivers and models tab

                refreshModelsTable();
                refresh();
                break;

        }
    }

    @Action
    public void importCSV() {
        final JFileChooser fc = new JFileChooser();

        int returnVal = fc.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            List<CSVHelper.ImportedDataObject> idos = CSVHelper.parseFile(file);

            Iterator<CSVHelper.ImportedDataObject> i = idos.iterator();
            while (i.hasNext()) {
                CSVHelper.ImportedDataObject ido = i.next();
                DBHelper.createNewDriverAndModel(ido.getDriverFirstName(), ido.getDriverSurname(),
                        ido.getCategoryNumber(), ido.getAmbId(), entityManager);
            }
            refresh();
        }

    }

    @Action(enabledProperty = "recordSelected")
    public void recordDriversNameVoice() {


        if (masterTable.getSelectedRow() >= 0) {

            String driversFilename = masterTable.getValueAt(masterTable.getSelectedRow(), 0) +
                    "_" + masterTable.getValueAt(masterTable.getSelectedRow(), 1);
//            statusLine.setText(msg("voicerecordingstarted"));
            if (driversNameRecordingInProgress) {
                audioManagerRecorder.stopRecording();
                recordDriversNameVoiceButton.setText(msg("startrecording"));
                audioManagerRecorder = null;
                driversNameRecordingInProgress = false;
            } else {
                audioManagerRecorder = new AudioManagerRecorder();
                audioManagerRecorder.prepareRecording(driversFilename);
                audioManagerRecorder.startRecording();
                recordDriversNameVoiceButton.setText(msg("stoprecording"));
                driversNameRecordingInProgress = true;
            }
        } else {
            // statusLine.setText(msg("SelectDriver"));
        }
    }

    @Action(enabledProperty = "recordSelected")
    public void playDriversNameVoice() {


        if (masterTable.getSelectedRow() >= 0) {

            String driversFilename = masterTable.getValueAt(masterTable.getSelectedRow(), 0) +
                    "_" + masterTable.getValueAt(masterTable.getSelectedRow(), 1);


            audioManagerPlayer = new AudioManagerPlayer(driversFilename);

            ((Thread) audioManagerPlayer).start();
        }
    }

    private void refreshCatsInModelTable() {


        TableColumn column = modelsTable.getColumnModel().getColumn(4);

        Vector categories = DBHelper.getAllCategories(entityManager);
        JComboBox comboBox = new JComboBox(categories);
        comboBox.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent e) {
                setSaveNeeded(true);
            }
        });
        column.setCellEditor(new DefaultCellEditor(comboBox));
    }

    private void refreshModelsTable() {

        if (masterTable.getSelectedRow() < 0) {
            modelsTable.setModel(new ModelsTableModel());
            return;
        }


        try {
            Driver d = driverList.get(masterTable.convertRowIndexToModel(masterTable.getSelectedRow()));
            modelsTable.setModel(new ModelsTableModel(d.getId(), entityManager));

        } catch (ArrayIndexOutOfBoundsException x) {
        }

        if (modelsTable.getRowCount() > 0) {
            modelsTable.getSelectionModel().setSelectionInterval(0, 0);
        }
        refreshCatsInModelTable();
    }

    private void saveModelsTable(int column, int firstRow) {
        //  Object o = modelsTable.getValueAt(firstRow, column);
        Long id = (Long) modelsTable.getValueAt(firstRow, 5);
        Model m = new Model();


        if (id != null) {
            for (int i = 0; i < modelList.size(); i++) {
                if (modelList.get(i).getId() == id) {
                    m = modelList.remove(i);
                }
            }
        } else {
            m = new Model();
        }

        m.setModelName((String) modelsTable.getValueAt(firstRow, 0));
        m.setFrequency1((String) modelsTable.getValueAt(firstRow, 1));
        m.setIdentificationType((String) modelsTable.getValueAt(firstRow, 2));
        m.setIdentificationId((String) modelsTable.getValueAt(firstRow, 3));
        m.setCategory((Long) modelsTable.getValueAt(firstRow, 4));
        modelList.add(m);
        setSaveNeeded(true);

    }

    public void setCategoryRecordSelected(boolean b) {
        boolean old = isCategoryRecordSelected();

        this.categoryRecordSelected = b;
        firePropertyChange("categoryRecordSelected", old, isCategoryRecordSelected());
    }

    private void setName(String string) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean isRecordSelected() {
        return masterTable.getSelectedRow() != -1;
    }

    public boolean isCategoryRecordSelected() {
        return categoryTable.getSelectedRow() != -1;
    }

    public boolean isRaceRecordSelected() {
        return raceTable.getSelectedRow() != -1;
    }

    public boolean isDetailRecordSelected() {
        return modelsTable.getSelectedRow() != -1;
    }

    @Action
    public void newRecord() {
        net.jlaps.entity.Driver d = new net.jlaps.entity.Driver();
        entityManager.persist(d);
        driverList.add(d);
        int row = driverList.size() - 1;
        masterTable.setRowSelectionInterval(row, row);
        masterTable.scrollRectToVisible(masterTable.getCellRect(row, 0, true));
        setSaveNeeded(true);
    }

    @Action
    public void newRace() {
        if ( categoryRaceList.getModel().getSize() == 0 )
            return;
        
        net.jlaps.entity.Race r = new net.jlaps.entity.Race();
        entityManager.persist(r);
        raceList.add(r);
        int row = raceList.size() - 1;
        raceTable.setRowSelectionInterval(row, row);
        raceTable.scrollRectToVisible(raceTable.getCellRect(row, 0, true));
        setSaveNeeded(true);
    }

    @Action
    public void newCategory() {
        net.jlaps.entity.Category r = new net.jlaps.entity.Category();
        entityManager.persist(r);
        categoryList.add(r);
        int row = categoryList.size() - 1;
        categoryTable.setRowSelectionInterval(row, row);
        categoryTable.scrollRectToVisible(categoryTable.getCellRect(row, 0, true));
        setSaveNeeded(true);
    }

    @Action(enabledProperty = "recordSelected")
    public void newDetailRecord() {

        int index = masterTable.getSelectedRow();
        net.jlaps.entity.Driver d = driverList.get(masterTable.convertRowIndexToModel(index));

        Model m = new Model();
        m.setDriverId(d.getId());
        ModelsTableModel mtm = (ModelsTableModel) modelsTable.getModel();
        mtm.addRecord(m);

        int row = mtm.getRowCount() - 1;

        modelsTable.setRowSelectionInterval(row, row);
        modelsTable.scrollRectToVisible(modelsTable.getCellRect(row, 0, true));
        modelsTable.repaint();
        setSaveNeeded(true);
    }


    @Action(enabledProperty = "categoryRecordSelected")
    public void deleteCategoryRecord() {
        int[] selected = categoryTable.getSelectedRows();
        List<net.jlaps.entity.Category> toRemove = new ArrayList<net.jlaps.entity.Category>(selected.length);
        for (int idx = 0; idx < selected.length; idx++) {
            net.jlaps.entity.Category c = categoryList.get(categoryTable.convertRowIndexToModel(selected[idx]));
            toRemove.add(c);
            entityManager.remove(c);
        }
        categoryList.removeAll(toRemove);
        setSaveNeeded(true);
    }
    @Action(enabledProperty = "raceRecordSelected")
    public void deleteRaceRecord() {

        int option = JOptionPane.showConfirmDialog(null, msg("Areyousuretodeletetheraceandraceresults"));
        if (option != JOptionPane.YES_OPTION) {
            return;
        }

        enableRaceTableListeners(false);
        synchronized (this) {
            int[] selected = raceTable.getSelectedRows();
//        List<net.jlaps.entity.Race> toRemove = new ArrayList<net.jlaps.entity.Race>(selected.length);

            for (int idx = 0; idx < selected.length; idx++) {
                net.jlaps.entity.Race r = raceList.get(raceTable.convertRowIndexToModel(selected[idx]));
//            toRemove.add(r);
                entityManager.remove(r);
                raceList.remove(r);
                Query q = entityManager.createNativeQuery("DELETE FROM DEVICE_EVENT e where e.race_id = :race_id");
                q.setParameter("race_id", r.getId()).executeUpdate();
                q = entityManager.createNativeQuery("DELETE FROM RACE_RESULT r " +
                        "where r.race_id = :race_id");
                q.setParameter("race_id", r.getId()).executeUpdate();

            }
//        raceList.removeAll(toRemove);

            setTodaysRaceFromOutside(null);
            
            categoryRaceList.clearSelection();
            
            enableRaceTableListeners(true);

            setSaveNeeded(true);
        }
    }

    @Action(enabledProperty = "raceRecordSelected")
    public void setTodaysRace() {
        int selectedRace = raceTable.getSelectedRow();
        Race r = raceList.get(selectedRace);

        setTodaysRaceFromOutside(r);
    }

    private class SaveTask extends Task {

        SaveTask(org.jdesktop.application.Application app) {
            super(app);
        }

        @Override
        protected Void doInBackground() {


            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().commit();
                entityManager.getTransaction().begin();
            }
            return null;
        }

        @Override
        protected void finished() {
            setSaveNeeded(false);
        }
    }

    private class SaveRaceTask extends Task {

        SaveRaceTask(org.jdesktop.application.Application app) {
            super(app);
        }

        @Override
        protected Void doInBackground() {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().commit();
                entityManager.getTransaction().begin();
            }
            return null;
        }

        @Override
        protected void finished() {
            setSaveNeeded(false);
        }
    }

    private class SaveCategoryTask extends Task {

        SaveCategoryTask(org.jdesktop.application.Application app) {
            super(app);
        }

        @Override
        protected Void doInBackground() {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().commit();
                entityManager.getTransaction().begin();
            }
            return null;
        }

        @Override
        protected void finished() {
            setSaveNeeded(false);
        }
    }

    @Action(enabledProperty = "detailRecordSelected")
    public void deleteDetailRecord() {
        int index = masterTable.getSelectedRow();
        net.jlaps.entity.Driver d = driverList.get(masterTable.convertRowIndexToModel(index));
        int[] selected = modelsTable.getSelectedRows();
        ModelsTableModel mtm = (ModelsTableModel) modelsTable.getModel();
        for (int i = 0; i < selected.length; i++) {
            mtm.removeRecord(selected[i]);
        }
        modelsTable.clearSelection();
        modelsTable.repaint();
        setSaveNeeded(true);
    }

    @Action(enabledProperty = "saveNeeded")
    public Task save() {
        return new SaveTask(getApplication());
    }

    @Action(enabledProperty = "saveNeeded")
    public Task saveRace() {
        return new SaveRaceTask(getApplication());
    }

    @Action(enabledProperty = "saveNeeded")
    public Task saveCategory() {
        return new SaveCategoryTask(getApplication());
    }

    @Action
    public Task refresh() {
        RefreshTask t = new RefreshTask(getApplication());

        return t;
    }

    @Action
    public Task refreshRace() {
        RefreshRaceTask t = new RefreshRaceTask(getApplication());
        raceTable.getSelectionModel().setSelectionInterval(0, 1);
        return t;
    }

    @Action
    public Task refreshCategory() {
        RefreshCategoryTask t = new RefreshCategoryTask(getApplication());
        return t;
    }

    private void setTodaysRaceFromOutside(Race r) {
        view.setTodaysRaceFromOutside(r);
    }

    private class RefreshTask extends Task {

        RefreshTask(org.jdesktop.application.Application app) {
            super(app);
        }

        @Override
        protected Object doInBackground() {

            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            entityManager.getTransaction().begin();
            java.util.Collection data = driverQuery.getResultList();

            driverList.clear();
            driverList.addAll(data);

            refreshModelsTable();

            return null;
        }

        @Override
        protected void finished() {
            setSaveNeeded(false);
        }
    }

    private class RefreshRaceTask extends Task {

        RefreshRaceTask(org.jdesktop.application.Application app) {
            super(app);
        }

        @Override
        protected Void doInBackground() {

            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
                entityManager.getTransaction().begin();
            }

            
            java.util.Collection data = raceQuery.getResultList();

            enableRaceTableListeners(false);
            raceList.clear();
            raceList.addAll(data);
            enableRaceTableListeners(true);

            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
                entityManager.getTransaction().begin();
            }
            
            return null;
        }

        @Override
        protected void finished() {
            setSaveNeeded(false);
        }
    }

    private class RefreshCategoryTask extends Task {

        RefreshCategoryTask(org.jdesktop.application.Application app) {
            super(app);
        }

        @Override
        protected Void doInBackground() {

            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
                entityManager.getTransaction().begin();                
            }

            java.util.Collection data = categoryQuery.getResultList();

            categoryList.clear();
            categoryList.addAll(data);

            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
                entityManager.getTransaction().begin();
  
            }
            
            
            return null;
        }

        @Override
        protected void finished() {
            setSaveNeeded(false);
        }
    }

    /**
     * enables and disables race table and category list listeners
     * @param b true - enable, false - disable
     */
    private void enableRaceTableListeners(boolean b) {

        if (b) {
            rtl = new RaceSetupView.raceTableLlistener();
            cll = new RaceSetupView.categoryListListener();
            raceTable.getSelectionModel().addListSelectionListener(rtl);
            categoryRaceList.getSelectionModel().addListSelectionListener(cll);
        } else {
            raceTable.getSelectionModel().removeListSelectionListener(rtl);
            categoryRaceList.getSelectionModel().removeListSelectionListener(cll);
        }

    }

    /**
     * waits for the category change. Sets the value into the race table.
     * construct cat;cat;cat string
     * ///TBD: parsing original string of groups, not 1;1;1 all the time
     */
    private class categoryListListener implements ListSelectionListener {

        public categoryListListener() {
        }

        public void valueChanged(ListSelectionEvent e) {

            /// sets categories values to races
            if (raceTable.getSelectedRow() < 0) {
                return;
            }

            String groupsString = (String) raceTable.getValueAt(raceTable.getSelectedRow(), 4);
            StringTokenizer stG;
            if (groupsString != null) {
                stG = new StringTokenizer(groupsString, ";");
            } else {
                stG = new StringTokenizer("", ";");
            }

            int[] ind = categoryRaceList.getSelectedIndices();
            StringBuffer cats = new StringBuffer();
            String adds = null;
            String categoyId;
            String oneGroupOrgValue;
            String groups = groupsString;
            for (int i = 1; i <= ind.length; i++) {
                if (stG.hasMoreTokens()) {
                    oneGroupOrgValue = stG.nextToken();
                } else if (groupsString != null) {
                    oneGroupOrgValue = groupsString;
                } /// just one category with one group present
                else {
                    oneGroupOrgValue = "1";
                }
                categoyId = ((Category) categoryRaceList.getModel().getElementAt(ind[i - 1])).getId().toString();
                if (adds == null) {
                    adds = categoyId;
                    groups = oneGroupOrgValue;
                } else {
                    adds = ";" + categoyId;
                    groups += ";" + oneGroupOrgValue;
                }
                cats.append(adds);
            }
            TableModel tm = raceTable.getModel();
            tm.setValueAt(cats.toString(), raceTable.getSelectedRow(), 3);
            tm.setValueAt(groups, raceTable.getSelectedRow(), 4);


        }
    }

    /**
     * checks changes of clicks in model table. Highlites related categories
     * in the categories list
     */
    private class modelTableLlistener implements ListSelectionListener {

        public modelTableLlistener() {
        }

        public void valueChanged(ListSelectionEvent e) {

            modelsTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
            if (modelsTable.getSelectedRow() == -1) {
                return;
            }

            TableModel tm = modelsTable.getModel();
            if (tm.getRowCount() == 0) {
                return;
            }
            Long category = (Long) modelsTable.getValueAt(modelsTable.getSelectedRow(), 4);

            setSaveNeeded(true);
        }
    }

    private class raceTableLlistener implements ListSelectionListener {

        public raceTableLlistener() {
        }

        public void valueChanged(ListSelectionEvent e) {

            List<Integer> indList = new ArrayList<Integer>();

            if (raceTable.getSelectedRow() == -1) {
                return;
            }

            if (raceList.size() == 0) {
                return;
            }

            Race r = raceList.get(raceTable.getSelectedRow());

            List raceCats = RaceHelper.getCategoriesByIDs(r.getCategories());

            CategoryListModel allCats = (CategoryListModel) categoryRaceList.getModel();
            Iterator<Category> x = allCats.getCategories().iterator();
            Category c;
            for (int i = 0; i < allCats.getSize(); i++) {
                c = x.next();
                if (raceCats.contains(c)) {
                    indList.add(i);
                }

            }
            int[] ind = new int[indList.size()];
            Iterator<Integer> iter = indList.iterator();
            for (int i = 0; i < indList.size(); i++) {
                ind[i] = iter.next();
            }
            categoryRaceList.setSelectedIndices(ind);

        }
    }

    /**
     * sets model category to the model table, column caegory
     * @param selectedItem selected category to be set
     */
    public void setModelsCategory(Object selectedItem) {
        int selectedRow = modelsTable.getSelectedRow();

        modelsTable.getModel().setValueAt((String) selectedItem, selectedRow, 4);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        mainPanel = new javax.swing.JPanel();
        tabbedPane = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        raceTable = new javax.swing.JTable();
        newRace = new javax.swing.JButton();
        saveRaceButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        categoryRaceList = new javax.swing.JList();
        jButton3 = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        categoryTable = new javax.swing.JTable();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        deleteDetailButton = new javax.swing.JButton();
        newDriver = new javax.swing.JButton();
        newDetailButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        masterScrollPane = new javax.swing.JScrollPane();
        masterTable = new javax.swing.JTable();
        saveButton = new javax.swing.JButton();
        detailScrollPane = new javax.swing.JScrollPane();
        modelsTable = new javax.swing.JTable();
        refreshButton = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        searchDriverField = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jButton18 = new javax.swing.JButton();
        recordDriversNameVoiceButton = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        entityManager = java.beans.Beans.isDesignTime() ? null : javax.persistence.Persistence.createEntityManagerFactory(DBHelper.getPMName()).createEntityManager();
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(net.jlaps.JLapsApp.class).getContext().getResourceMap(RaceSetupView.class);
        raceQuery = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery(resourceMap.getString("raceQuery.query")); // NOI18N
        raceList = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(raceQuery.getResultList());
        categoryQuery = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery(resourceMap.getString("categoryQuery.query")); // NOI18N
        categoryList = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(categoryQuery.getResultList());
        driverQuery = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery(resourceMap.getString("driverQuery.query")); // NOI18N
        driverList = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(driverQuery.getResultList());
        modelQuery = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery(resourceMap.getString("modelQuery.query")); // NOI18N
        modelList = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(modelQuery.getResultList());

        mainPanel.setAutoscrolls(true);
        mainPanel.setName("mainPanel"); // NOI18N

        tabbedPane.setName("tabbedPane"); // NOI18N
        tabbedPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tabbedPaneStateChanged(evt);
            }
        });

        jPanel1.setName("jPanel1"); // NOI18N

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        raceTable.setName("raceTable"); // NOI18N

        org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, raceList, raceTable);
        org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${name}"));
        columnBinding.setColumnName("Name");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${raceDate}"));
        columnBinding.setColumnName("Race Date");
        columnBinding.setColumnClass(java.util.Date.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${proposition}"));
        columnBinding.setColumnName("Proposition");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${categories}"));
        columnBinding.setColumnName("Categories");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${groups}"));
        columnBinding.setColumnName("Groups");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${minLapTime}"));
        columnBinding.setColumnName("Min Lap Time");
        columnBinding.setColumnClass(Integer.class);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        raceTable.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                raceTableFocusGained(evt);
            }
        });
        jScrollPane2.setViewportView(raceTable);
        raceTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("raceTable.columnModel.title0")); // NOI18N
        raceTable.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("raceTable.columnModel.title1")); // NOI18N
        raceTable.getColumnModel().getColumn(2).setHeaderValue(resourceMap.getString("raceTable.columnModel.title2")); // NOI18N
        raceTable.getColumnModel().getColumn(3).setHeaderValue(resourceMap.getString("raceTable.columnModel.title3")); // NOI18N
        raceTable.getColumnModel().getColumn(4).setHeaderValue(resourceMap.getString("raceTable.columnModel.title4")); // NOI18N
        raceTable.getColumnModel().getColumn(5).setHeaderValue(resourceMap.getString("raceTable.columnModel.title5")); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(net.jlaps.JLapsApp.class).getContext().getActionMap(RaceSetupView.class, this);
        newRace.setAction(actionMap.get("newRace")); // NOI18N
        newRace.setName("newRace"); // NOI18N

        saveRaceButton.setAction(actionMap.get("saveRace")); // NOI18N
        saveRaceButton.setName("saveRaceButton"); // NOI18N

        jButton1.setAction(actionMap.get("refreshRace")); // NOI18N
        jButton1.setName("jButton1"); // NOI18N

        jButton2.setAction(actionMap.get("deleteRaceRecord")); // NOI18N
        jButton2.setName("jButton2"); // NOI18N

        jScrollPane3.setName("jScrollPane3"); // NOI18N

        categoryRaceList.setName("categoryRaceList"); // NOI18N
        jScrollPane3.setViewportView(categoryRaceList);

        jButton3.setAction(actionMap.get("setTodaysRace")); // NOI18N
        jButton3.setName("jButton3"); // NOI18N

        jLabel16.setName("jLabel16"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jScrollPane2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                            .addComponent(newRace, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                            .addComponent(saveRaceButton, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                            .addComponent(jButton3))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(newRace)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addGap(12, 12, 12)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(saveRaceButton)
                        .addGap(40, 40, 40)
                        .addComponent(jButton3)
                        .addGap(25, 25, 25))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabbedPane.addTab(resourceMap.getString("jPanel1.TabConstraints.tabTitle"), jPanel1); // NOI18N

        jPanel6.setName("jPanel6"); // NOI18N

        jScrollPane5.setName("jScrollPane5"); // NOI18N

        categoryTable.setName("categoryTable"); // NOI18N

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, categoryList, categoryTable);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${name}"));
        columnBinding.setColumnName("Name");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${bestHeats}"));
        columnBinding.setColumnName("Best Heats");
        columnBinding.setColumnClass(Integer.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${heats}"));
        columnBinding.setColumnName("Heats");
        columnBinding.setColumnClass(Integer.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${bestFinals}"));
        columnBinding.setColumnName("Best Finals");
        columnBinding.setColumnClass(Integer.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${finals}"));
        columnBinding.setColumnName("Finals");
        columnBinding.setColumnClass(Integer.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${calculatorName}"));
        columnBinding.setColumnName("Calculator Name");
        columnBinding.setColumnClass(String.class);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        jScrollPane5.setViewportView(categoryTable);
        categoryTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("categoryTable.columnModel.title0")); // NOI18N
        categoryTable.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("categoryTable.columnModel.title1")); // NOI18N
        categoryTable.getColumnModel().getColumn(2).setHeaderValue(resourceMap.getString("categoryTable.columnModel.title2")); // NOI18N
        categoryTable.getColumnModel().getColumn(3).setHeaderValue(resourceMap.getString("categoryTable.columnModel.title3")); // NOI18N
        categoryTable.getColumnModel().getColumn(4).setHeaderValue(resourceMap.getString("categoryTable.columnModel.title4")); // NOI18N
        categoryTable.getColumnModel().getColumn(5).setHeaderValue(resourceMap.getString("categoryTable.columnModel.title5")); // NOI18N

        jButton4.setAction(actionMap.get("newCategory")); // NOI18N
        jButton4.setName("jButton4"); // NOI18N

        jButton5.setAction(actionMap.get("saveCategory")); // NOI18N
        jButton5.setName("jButton5"); // NOI18N

        jButton6.setAction(actionMap.get("deleteCategoryRecord")); // NOI18N
        jButton6.setName("jButton6"); // NOI18N

        jButton7.setAction(actionMap.get("refreshCategory")); // NOI18N
        jButton7.setName("jButton7"); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jButton4)
                        .addGap(54, 54, 54)
                        .addComponent(jButton6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton7)))
                .addGap(758, 758, 758))
        );

        tabbedPane.addTab(resourceMap.getString("jPanel6.TabConstraints.tabTitle"), jPanel6); // NOI18N

        jPanel3.setName("jPanel3"); // NOI18N

        deleteDetailButton.setAction(actionMap.get("deleteDetailRecord")); // NOI18N
        deleteDetailButton.setName("deleteDetailButton"); // NOI18N

        newDriver.setAction(actionMap.get("newRecord")); // NOI18N
        newDriver.setName("newDriver"); // NOI18N

        newDetailButton.setAction(actionMap.get("newDetailRecord")); // NOI18N
        newDetailButton.setName("newDetailButton"); // NOI18N

        deleteButton.setAction(actionMap.get("deleteRecord")); // NOI18N
        deleteButton.setName("deleteButton"); // NOI18N

        masterScrollPane.setName("masterScrollPane"); // NOI18N

        masterTable.setName("masterTable"); // NOI18N

        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, driverList, masterTable);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${firstName}"));
        columnBinding.setColumnName("First Name");
        columnBinding.setColumnClass(String.class);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${secondName}"));
        columnBinding.setColumnName("Second Name");
        columnBinding.setColumnClass(String.class);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();
        masterScrollPane.setViewportView(masterTable);

        saveButton.setAction(actionMap.get("save")); // NOI18N
        saveButton.setName("saveButton"); // NOI18N

        detailScrollPane.setName("detailScrollPane"); // NOI18N

        modelsTable.setModel(new ModelsTableModel());
        modelsTable.setName("modelsTable"); // NOI18N
        modelsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                modelsTableMouseClicked(evt);
            }
        });
        modelsTable.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                modelsTableFocusGained(evt);
            }
        });
        detailScrollPane.setViewportView(modelsTable);

        refreshButton.setAction(actionMap.get("refresh")); // NOI18N
        refreshButton.setName("refreshButton"); // NOI18N

        jLabel4.setName("jLabel4"); // NOI18N

        jLabel5.setName("jLabel5"); // NOI18N

        jLabel15.setName("jLabel15"); // NOI18N

        searchDriverField.setName("searchDriverField"); // NOI18N
        searchDriverField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchDriverFieldActionPerformed(evt);
            }
        });
        searchDriverField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchDriverFieldKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchDriverFieldKeyReleased(evt);
            }
        });

        jLabel20.setName("jLabel20"); // NOI18N

        jButton18.setAction(actionMap.get("importCSV")); // NOI18N
        jButton18.setName("jButton18"); // NOI18N

        recordDriversNameVoiceButton.setAction(actionMap.get("recordDriversNameVoice")); // NOI18N
        recordDriversNameVoiceButton.setName("recordDriversNameVoiceButton"); // NOI18N

        jButton19.setAction(actionMap.get("playDriversNameVoice")); // NOI18N
        jButton19.setName("jButton19"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(detailScrollPane, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(searchDriverField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(masterScrollPane, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(deleteButton, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                            .addComponent(newDriver, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                            .addComponent(saveButton, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                            .addComponent(recordDriversNameVoiceButton, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                            .addComponent(jButton19)
                            .addComponent(refreshButton, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton18))
                        .addContainerGap())
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(deleteDetailButton, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                            .addComponent(newDetailButton, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel15)
                        .addGap(167, 167, 167))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(searchDriverField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(masterScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 377, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(newDriver, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(recordDriversNameVoiceButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton19)
                        .addGap(28, 28, 28)
                        .addComponent(refreshButton, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(newDetailButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(deleteDetailButton)
                        .addGap(31, 31, 31)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(detailScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel15))
                .addGap(312, 312, 312))
        );

        tabbedPane.addTab(resourceMap.getString("jPanel3.TabConstraints.tabTitle"), jPanel3); // NOI18N

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 679, Short.MAX_VALUE)
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, 686, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        setComponent(mainPanel);

        bindingGroup.bind();
    }// </editor-fold>//GEN-END:initComponents
    private void raceTableFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_raceTableFocusGained
    }//GEN-LAST:event_raceTableFocusGained

    private void modelsTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modelsTableMouseClicked
    }//GEN-LAST:event_modelsTableMouseClicked

    private void modelsTableFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_modelsTableFocusGained
    }//GEN-LAST:event_modelsTableFocusGained

    private void searchDriverFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchDriverFieldActionPerformed
    }//GEN-LAST:event_searchDriverFieldActionPerformed

    private void searchDriverFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchDriverFieldKeyPressed
    }//GEN-LAST:event_searchDriverFieldKeyPressed

    private void searchDriverFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchDriverFieldKeyReleased
        TableRowSorter<TableModel> sorter = (TableRowSorter) masterTable.getRowSorter();
        try {
            if (searchDriverField.getText() == null || searchDriverField.getText().length() == 0) {
                sorter.setRowFilter(null);
                System.out.println("No row filter");
            } else {
                sorter.setRowFilter(RowFilter.regexFilter(searchDriverField.getText()));
                System.out.println("Row filter:" + searchDriverField.getText());
            }
        } catch (PatternSyntaxException pse) {
            sorter.setRowFilter(null);
        }
    }//GEN-LAST:event_searchDriverFieldKeyReleased

private void tabbedPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tabbedPaneStateChanged
    
}//GEN-LAST:event_tabbedPaneStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.util.List<net.jlaps.entity.Category> categoryList;
    private javax.persistence.Query categoryQuery;
    private javax.swing.JList categoryRaceList;
    private javax.swing.JTable categoryTable;
    private javax.swing.JButton deleteButton;
    private javax.swing.JButton deleteDetailButton;
    private javax.swing.JScrollPane detailScrollPane;
    private java.util.List<net.jlaps.entity.Driver> driverList;
    private javax.persistence.Query driverQuery;
    private javax.persistence.EntityManager entityManager;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JScrollPane masterScrollPane;
    private javax.swing.JTable masterTable;
    private java.util.List<net.jlaps.entity.Model> modelList;
    private javax.persistence.Query modelQuery;
    private javax.swing.JTable modelsTable;
    private javax.swing.JButton newDetailButton;
    private javax.swing.JButton newDriver;
    private javax.swing.JButton newRace;
    private java.util.List<net.jlaps.entity.Race> raceList;
    private javax.persistence.Query raceQuery;
    private javax.swing.JTable raceTable;
    private javax.swing.JButton recordDriversNameVoiceButton;
    private javax.swing.JButton refreshButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JButton saveRaceButton;
    private javax.swing.JTextField searchDriverField;
    private javax.swing.JTabbedPane tabbedPane;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
//    private final Timer messageTimer;
//    private final Timer busyIconTimer;
//    private final Icon idleIcon;
//    private final Icon[] busyIcons = new Icon[15];
//    private int busyIconIndex = 0;
    private boolean saveNeeded;
}
