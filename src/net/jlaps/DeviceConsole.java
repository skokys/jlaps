/*
 * This is an interface for device consoles. Implemented classes has to provide
 * adding of events and setting console visible
 */

package net.jlaps;

import java.util.Date;

/**
 * interface for device consoles
 * @author lskokan
 */
public interface DeviceConsole {
    /**
     * adds new event to console
     * @param event
     * @param dateTime
     */
    void addEvent(String event, Date dateTime);
    
    /**
     * sets the console visible
     * @param aFlag
     */
    public void setVisible(boolean aFlag);
}
