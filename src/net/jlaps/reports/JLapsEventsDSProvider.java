/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.reports;

import java.util.ArrayList;
import java.util.Date;
import net.jlaps.entity.Event;

/**
 *
 * @author lskokan 
 */
public class JLapsEventsDSProvider extends JLapsDSProvider {

    public JLapsEventsDSProvider() {
        super(Event.class);
        getSampleData();

    }

    
    private void getSampleData() {
        list = new ArrayList();
        Event e = new Event();
        e.setIdentificationId("78787");
        e.setTimestamp(new Date());
        list.add(e);
        e.setIdentificationId("78787");
        e.setTimestamp(new Date());
        list.add(e);
        e.setIdentificationId("21223");
        e.setTimestamp(new Date());
        list.add(e);        
        e.setIdentificationId("21223");
        e.setTimestamp(new Date());

        list.add(e);
    }    

}
