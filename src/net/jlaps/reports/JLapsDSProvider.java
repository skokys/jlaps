/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jlaps.reports;

import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author lskokan
 */
public abstract class JLapsDSProvider extends JRAbstractBeanDataSourceProvider {
 
    protected List list;
    JRDataSource source;
    
    public JLapsDSProvider() {
        super(Object.class);
    }
    
    public JLapsDSProvider(Class c) {
        super(c);
    }
    
    public void setValues(List values) {
        this.list = values; 
    }
        
  
    public JRDataSource create(JasperReport arg0) throws JRException {
        if ( source == null )
            source =  new JRBeanCollectionDataSource(list);
  	return source;
    }

    public void dispose(JRDataSource arg0) throws JRException {
        // nothing
    }

    public JRDataSource getSource() {
        if ( source == null )
            source =  new JRBeanCollectionDataSource(list);
        return source;
    }
    
    
}

