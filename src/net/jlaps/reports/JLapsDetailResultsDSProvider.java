/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.reports;

import java.util.ArrayList;
import net.jlaps.entity.DetailResult;

/**
 *
 * @author lskokan 
 */
public class JLapsDetailResultsDSProvider extends JLapsDSProvider {

    public JLapsDetailResultsDSProvider() {
        super(DetailResult.class);
        getSampleData();

    }

    
    private void getSampleData() {
          list = new ArrayList();
          DetailResult d = new DetailResult();
          d.setDriverName("John Kenedy");
          d.setResultsString("Here to be detailed result string of all rounds");
          list.add(d);
    }    

}
