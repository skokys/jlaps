/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.reports;

import java.util.ArrayList;
import net.jlaps.entity.DriversAndIds;

/**
 *
 * @author lskokan
 */
public class JLapsDriversDSProvider extends JLapsDSProvider {

    public JLapsDriversDSProvider() {
        super(DriversAndIds.class);
        getSampleData();
    }

    private void getSampleData() {
        list = new ArrayList();
        DriversAndIds d = new DriversAndIds();
        d.setRaceName("Testing race");
        d.setCategoryName("testing category");
        d.setGroup("A");
        d.setDriverName("John Kenedy");
        d.setIdentificationId("34345345");
        list.add(d);

        d = new DriversAndIds();
        d.setRaceName("Testing race");
        d.setCategoryName("testing category");
        d.setGroup("B");
        d.setDriverName("John Kenedy Jr.");
        d.setIdentificationId("34345345");
        list.add(d);

        d = new DriversAndIds();
        d.setRaceName("Testing race");
        d.setCategoryName("second categry category");
        d.setGroup("A");
        d.setDriverName("Peter Pan");
        d.setIdentificationId("33345345");
        list.add(d);
        
        
    }
     
    

}
