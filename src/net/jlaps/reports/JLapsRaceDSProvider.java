/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.reports;

import java.util.ArrayList;
import java.util.Date;
import net.jlaps.entity.Race;

/**
 *
 * @author lskokan
 */
public class JLapsRaceDSProvider extends JLapsDSProvider {

    public JLapsRaceDSProvider() {
        super(Race.class);
        getSampleData();
    }

    private void getSampleData() {
        list = new ArrayList();
        Race r = new Race();
        r.setMinLapTime(10);
        r.setProposition("htttp://somwhere");
        r.setName("Testing dummy race");
        r.setRaceDate(new Date());
        list.add(r);
        
    }
     
    

}
