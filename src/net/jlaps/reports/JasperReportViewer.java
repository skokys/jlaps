package net.jlaps.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 * Servlet implementation class for Servlet: JasperReportViewer
 * 
 */
public class JasperReportViewer {
	static final long serialVersionUID = 1L;


    private void copyAllReportsToTemp() {
        
        String[] r = new String[]{"drivers_and_ids.jasper","rounds.jasper","detail_results.jasper","final_results.jasper","results.jasper"};
        for (int i=0;i<r.length;i++) {
            FileOutputStream fos = null;
            InputStream is = null;
            try {
                System.out.println("Copying report " + r[i]);
                is =  this.getClass().getResourceAsStream(r[i]);
                fos = new FileOutputStream(new File(TEMP_DIR + r[i]));
                byte[] b = new byte[1024];
                while (is.read(b) != -1) {
                    fos.write(b);
                }
            } catch (Exception ex) {
                Logger.getLogger(JasperReportViewer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fos.close();
                } catch (IOException ex) {
                    Logger.getLogger(JasperReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    is.close();
                } catch (IOException ex) {
                    Logger.getLogger(JasperReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        }
    }

	Connection con = null;
	DataSource ds = null;

	static String reportOutputDir = "out";
//	static String reportInputDir = "reports";
        static String reportInputDir = ".";
        static String reportViewerExe;
        
        static {
            File v = new File("c:\\Program Files\\Adobe\\Reader 8.0\\Reader\\AcroRd32.exe");
            if ( v.exists())
                reportViewerExe=v.getAbsolutePath();
            
        }
        private String separ = System.getProperty("file.separator");
        
        private String TEMP_DIR = System.getProperty("java.io.tmpdir");
        private String  BASE_DIR = TEMP_DIR;
        
        
	public JasperReportViewer()  {

                File f = new File(reportInputDir);
                if ( !f.exists()) f.mkdir();
                f = new File(reportOutputDir);
                if ( !f.exists()) f.mkdir();
                f=null;
                
                copyAllReportsToTemp();
	}




        public boolean printReport(String report,HashMap params, JRDataSource dp) {

                        String outputPdf = generateReport(report, params,dp);
                        
			if ( outputPdf != null ) {
                            viewReport(outputPdf);
                            return true;
                        } else
                            return false;

	}
 

	private String generateReport(String report, HashMap parameters, JRDataSource dp) {

                String reportName = report + ".jasper";
                InputStream is = this.getClass().getResourceAsStream(reportName);
                
		
                if ( parameters == null )
                    parameters = new HashMap();
                
                parameters.put("base_dir", BASE_DIR);
		
		try {

                        byte[] output  = JasperRunManager.runReportToPdf( is, parameters,dp);
                        
                        String exportedFile = TEMP_DIR + separ + report + System.currentTimeMillis() + ".pdf";
                        FileOutputStream exportedFileFos = new FileOutputStream(new File(exportedFile));
                        exportedFileFos.write(output);
                        exportedFileFos.close();

                        return exportedFile;

		} catch (Exception jre) {
			jre.printStackTrace();
			return null;

		} catch (Throwable x) {
			System.out.println("Unexpected exception here" + x);
			x.printStackTrace();
                        return null;
		} 
			

		

	}

    private void viewReport(String report) {
        try {
            
            String toExe = reportViewerExe + " " + report;
            System.out.println("To execute:" + toExe);
            Runtime.getRuntime().exec(toExe);

        } catch (IOException ex) {
            Logger.getLogger(JasperReportViewer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}