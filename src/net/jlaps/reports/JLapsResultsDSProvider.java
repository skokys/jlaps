/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jlaps.reports;

import java.util.ArrayList;
import net.jlaps.entity.RaceResult;

/**
 *
 * @author lskokan
 */
public class JLapsResultsDSProvider extends JLapsDSProvider {

    public JLapsResultsDSProvider() {
        super(RaceResult.class);
        getSampleData();
    }

    private void getSampleData() {
        list = new ArrayList();
        RaceResult r = new RaceResult((long)1,(long)2,"A",new int[]{1,0});
        r.setBestRoundTime((float)10);
        r.setCompleteRoundsAchieved(20);
        r.setPosition(1);
        r.setStartingPosition(10);
        r.setFinishTime(new Long(3000));
        list.add(r);
        
    }
     
    

}
